// API properties
export const REST_API_BASE_URL = "http://192.168.0.101:8080/api";

export const REST_API_USER_URL = "/user";
export const REST_API_LOGIN_URL = REST_API_USER_URL + "/login";
export const REST_API_REGISTER_URL = REST_API_USER_URL + "/register";
export const REST_API_SET_CURRENT_TRIP_URL = REST_API_USER_URL + "/set-active-trip";

export const REST_API_TRIP_URL = "/trip";
export const REST_API_TRIP_CURRENCIES_SUFFIX_URL = "/currencies";
export const REST_API_TRIP_PARTICIPANTS_SUFFIX_URL = "/participants";
export const REST_API_TRIPS_OF_USER_URL = REST_API_TRIP_URL + "/user";
export const REST_API_CREATE_TRIP_URL = REST_API_TRIP_URL + "/for-user";

export const REST_API_TRANSACTION_URL = "/transaction";

// Redux action names
export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
export const SET_CURRENT_TRIP = "SET_CURRENT_TRIP";
export const RESET_STATE = "RESET_STATE";

// UI
export const ERROR_INPUT_COLOR = "red";
export const VALID_INPUT_COLOR = "#e6e6e6";
