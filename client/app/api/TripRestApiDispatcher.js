import { setCurrentTrip, setUserTrips } from '../state/Actions';
import AbstractRestApiDispatcher from './AbstractRestApiDispatcher';
import * as Constants from '../constants/Constants';

export default class TripRestApiDispatcher extends AbstractRestApiDispatcher {

    static async getCurrentTrip(url, store) {
        AbstractRestApiDispatcher.executeGetRequest(url)
            .then(response => response.json())
            .then((currentTrip) => {
                if (currentTrip !== undefined) {
                    store.dispatch( setCurrentTrip ({ currentTrip: currentTrip }) );
                    return currentTrip;
                } else {
                    store.dispatch( setCurrentTrip ({ currentTrip: undefined }) );
                    return undefined;
                }
            })
            .catch(function(error) {
                console.log(error);
                return undefined;
            });
    }

    static async setCurrentTripForUser(baseUrl, userId, tripId, store) {
        AbstractRestApiDispatcher.executeGetRequest(baseUrl + "/" + userId + "/" + tripId)
            .then(response => response.json())
            .then((updatedUser) => {
                if (updatedUser !== undefined) {
                    TripRestApiDispatcher.getCurrentTrip(
                        Constants.REST_API_BASE_URL + Constants.REST_API_TRIP_URL + "/" + updatedUser.activeTripId, store);
                } else {
                    throw 'empty response';
                }
            })
            .catch(function(error) {
                console.log(error);
                return undefined;
            });
    }

    static async createTrip(url, trip) {
        return AbstractRestApiDispatcher.executePostRequest(url, trip);
    }
}
