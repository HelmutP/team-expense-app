import { setCurrentTrip } from '../state/Actions';

export default class AbstractRestApiDispatcher {

    static async getRequest(url) {
        return AbstractRestApiDispatcher.executeGetRequest(url)
            .then(response => response.json())
            .catch(function(error) {
                console.log(error);
        });    
    }

    static async executePostRequest(url, bodyObj) {
        return fetch(url, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(bodyObj)
        });
    }

    static async executeGetRequest(url) {
        return fetch(url, {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' }
        });
    }
}
