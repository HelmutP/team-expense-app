import AuthenticationGuard from '../authentication/AuthenticationGuard';
import AbstractRestApiDispatcher from './AbstractRestApiDispatcher';
import { login } from '../state/Actions';
import * as Constants from '../constants/Constants';
import TripRestApiDispatcher from '../api/TripRestApiDispatcher';

export default class AuthenticationRestApiDispatcher extends AbstractRestApiDispatcher {

    static async register(url, store, componentContext, username, password, iban, displayName) {

        AbstractRestApiDispatcher.executePostRequest(url, { userName: username,
            password: AuthenticationGuard.getPasswordHash(password), iban: iban, displayName: displayName }
        )
        .then(response => response.json())
        .then((user) => {
            if (user) {
                console.log("User " + user.displayName + "has been successfully registered");
            } else {
                throw 'empty response';
            }
            componentContext.setState({snackBar: {visible: true, text: 'Registration successful, now you can login.', actionText: 'Login'}});
        })
        .catch(function (error) {
            console.log(error);
            componentContext.setState({snackBar: {visible: true, text: 'Registration not successful.'}, actionText: 'OK'});
        });
    }

    static async login(url, store, componentContext, username, password) {

        AbstractRestApiDispatcher.executePostRequest(
            url, {userName: username, password: AuthenticationGuard.getPasswordHash(password)}
        )
        .then(response => response.json())
        .then((user) => {
            if (user && user.userName && user.displayName) {

                console.log("User " + JSON.stringify(user) + " has been successfully logged in.");
                componentContext.clearForm();

                store.dispatch(login(
                    { id: user.id.toString() }
                ));

                TripRestApiDispatcher.getCurrentTrip(
                    Constants.REST_API_BASE_URL + Constants.REST_API_TRIP_URL + "/" + user.activeTripId, store);

                componentContext.redirectIfLoggedIn();
            } else {
                throw 'empty response';
            }
        })
        .catch(function (error) {
            console.log(error);
            componentContext.setState({snackBar: {visible: true, text: 'Login not successful.'}});
        });
    }
}
