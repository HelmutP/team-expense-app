import AbstractRestApiDispatcher from './AbstractRestApiDispatcher';

export default class TransactionRestApiDispatcher extends AbstractRestApiDispatcher {

    static async addTransaction(url, transaction) {
        return AbstractRestApiDispatcher.executePostRequest(url, transaction);
    }
}
