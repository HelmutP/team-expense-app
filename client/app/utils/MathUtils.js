import * as BaseUtils from './BaseUtils';

export function roundOnTwoDecimalPlaces(number) {
    if (BaseUtils.isNumber) {
        return Math.round(number * 100) / 100;
    } else {
        return undefined;
    }
}
