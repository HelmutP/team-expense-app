export function isNumber(text) {
    if (text !== undefined && text.length > 0) {
        if (!isNaN(Number(text))) {
            return true;
        }
    }
    return false;
}
