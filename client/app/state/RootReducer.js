import { LOGIN, LOGOUT, SET_CURRENT_TRIP, RESET_STATE } from "../constants/Constants";

const initialState = {
    user: {
        isLoggedIn: false,
        id: undefined
    },
    currentTrip: undefined
};

function rootReducer(state = initialState, action) {

    if (action.type === LOGIN) {
        return {
            ...state,
            user: {...state.user, id: action.payload.id, isLoggedIn: true},
            currentTrip: undefined
        }
    }
    if (action.type === LOGOUT) {
        return {
            ...state,
            user: {...state.user, id: undefined, isLoggedIn: false},
            currentTrip: undefined
        }
    }
    if (action.type === SET_CURRENT_TRIP) {
        return {
            ...state,
            currentTrip: action.payload.currentTrip
        }
    }
    if (action.type === RESET_STATE) {
        return {...initialState}
    }

    return state;
}

export default rootReducer;
