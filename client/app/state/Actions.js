import { LOGIN, LOGOUT, SET_CURRENT_TRIP, RESET_STATE } from "../constants/Constants";

export function login(payload) {
    return { type: LOGIN, payload };
}
export function logout() {
    return { type: LOGOUT };
}
export function setCurrentTrip(payload) {
    return { type: SET_CURRENT_TRIP, payload };
}
export function resetState() {
    return { type: RESET_STATE };
}
