import { StyleSheet } from 'react-native';

export const RESTRICTED_ACCESS_STYLES = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ededed',
        marginTop: 20
    },
    contentContainer: {
        flex:1,
        padding: 5
    },
    title: {
        fontWeight: 'bold',
        fontSize: 18,
    },
    subtitle: {
        fontWeight: 'bold',
        fontSize: 16,
        padding: 5
    },
    titleBox: {
        padding: 5,
        marginBottom: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    input: {
        height: 50,
        borderColor: '#e6e6e6',
        padding: 5,
        borderWidth: 1,
        backgroundColor: 'white',
        marginBottom: 5
    },
    inputRowBox: {
        flexGrow: 1,
        display: 'flex',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignContent: 'center'
    },
    textArea: {
        borderColor: '#e6e6e6',
        padding: 5,
        borderWidth: 1,
        backgroundColor: 'white',
        marginBottom: 5
    },
    addButton: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'green'
    },
    buttonContainer: {
        marginTop: 10,
        marginBottom: 10
    },
    boldText: {
        fontWeight: 'bold'
    }
});
