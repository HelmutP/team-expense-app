
import { StyleSheet } from 'react-native';

export const AUTH_STYLES = StyleSheet.create({
    container: {
        flex: 1
    },
    authContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ededed'
    },
    loginInput: {
        height: 40,
        width: '80%',
        borderColor: '#e6e6e6',
        padding: 5,
        margin: 5,
        borderWidth: 1,
        backgroundColor: 'white'
    },
    registerInput: {
        height: 40,
        width: '80%',
        borderColor: '#e6e6e6',
        padding: 5,
        margin: 5,
        borderWidth: 1,
        backgroundColor: 'white'
    },
    button: {
        width: '80%',
    },
    noAccountTitle: {
        fontSize: 14,
        marginTop: 40,
        marginBottom: 10
    },
    welcomeTitle: {
        fontWeight: 'bold',
        fontSize: 18,
        margin: 15,
    },
});
