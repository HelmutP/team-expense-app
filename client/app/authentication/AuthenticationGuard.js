import sha512 from 'crypto-js/sha512';
import { AsyncStorage } from 'react-native';
import * as Constants from '../constants/Constants';

import store from '../state/Store';
import { logout } from '../state/Actions';

export default class AuthenticationGuard {

    static getPasswordHash(rawPassword) {
        return sha512(rawPassword).toString();
    }

    static checkUserLoggedIn(navigation) {
        if (!store.getState().user.isLoggedIn) {
            navigation.navigate('Login');
        }
    }

    static logout(navigation) {
        store.dispatch( logout () );
        navigation.navigate('Login');
    }
}
