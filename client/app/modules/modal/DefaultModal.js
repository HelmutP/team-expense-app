import React, { Component } from 'react';
import { Platform, Text, View, Button, AppRegistry, StyleSheet, TextInput, Alert, Modal, TouchableHighlight } from 'react-native';

export default class DefaultModal extends Component {

    constructor(props){
        super(props);

        this.state = {
            visible: props.visible
        }
    }

    render() {
        return (
            <Modal animationType="slide" transparent={true} visible={this.state.visible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                }}
            >
            <View style={styles.centeredView}>
                <View style={styles.modalView}>
                    <TouchableHighlight style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
                            onPress={() => this.setState({visible:false})}
                    >
                        <Text style={styles.textStyle}>Cancel</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});

AppRegistry.registerComponent('DefaultModal', () => DefaultModal);
