import React, { Component } from 'react';
import { AppRegistry, Platform, View, StyleSheet, Text } from 'react-native';
import { Header } from 'react-native-elements';
import { Left, Right, Icon } from 'native-base';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import store from '../../state/Store';
import AbstractComponent from '../../screens/AbstractComponent';

export default class HeaderBar extends AbstractComponent {

    constructor(props) {
        super(props);

        this.state = {
            currentTripName: ''
        }

        this.subscriptions.push(
            this.store.subscribe(() => {
                this.setCurrentTripName();
            })
        );
    }

    componentDidMount() {
        this.setCurrentTripName();
    }

    setCurrentTripName() {
        if (this.store.getState().currentTrip != undefined) {
            this.setState({currentTripName: this.store.getState().currentTrip.name});
        } else {
            this.setState({currentTripName: undefined});
        }
    }

    render() {
        return (
            <View>
                <Header backgroundColor='#000' containerStyle={styles.header}
                    leftComponent={{
                        text: this.state.currentTripName !== undefined ? this.state.currentTripName : '',
                        style: styles.headerText
                    }}
                    rightComponent={
                        <Icon type="MaterialIcons" name="menu" style={styles.menuIcon}
                            onPress={() => this.props.navigation.openDrawer()} />
                    }
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        height: 60,
        paddingTop: 2
    },
    menuIcon: {
        color: '#fff',
        paddingRight: 5
    },
    headerText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 16,
        width: 600,
        paddingRight: 5
    }
});

AppRegistry.registerComponent('HeaderBar', () => HeaderBar);
