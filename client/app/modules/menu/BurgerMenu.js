import React, { Component } from 'react';
import { Platform, AppRegistry, ScrollView, TouchableOpacity, Text, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements'
import AuthenticationGuard from '../../authentication/AuthenticationGuard';
import AbstractComponent from '../../screens/AbstractComponent';

export default class BurgerMenu extends AbstractComponent {

    constructor(props) {
        super(props);
    }

    onLogoutClick() {
        AuthenticationGuard.logout(this.props.navigation);
    }
    
    render() {
        return(
            <ScrollView style={{marginTop: 20}}>
                <TouchableOpacity style = {styles.evenRow} onPress={() => {
                    this.props.navigation.navigate('Dashboard');
                    this.props.navigation.closeDrawer();
                }}>
                    <Icon name='home' type='material' />
                    <Text style = {styles.rowText}> Dashboard</Text>
                </TouchableOpacity>
                <TouchableOpacity style = {styles.oddRow} onPress={() => {
                    this.props.navigation.navigate('TripSettings');
                    this.props.navigation.closeDrawer();
                }}>
                    <Icon name='settings' type='material' />
                    <Text style = {styles.rowText}> Trip settings</Text>
                </TouchableOpacity>
                <TouchableOpacity style = {styles.evenRow} onPress={() => {
                    this.props.navigation.navigate('SwitchTrip');
                    this.props.navigation.closeDrawer();
                }}>
                    <Icon name='cached' type='material' />
                    <Text style = {styles.rowText}> Switch trip</Text>
                </TouchableOpacity>
                <TouchableOpacity style = {styles.oddRow} onPress={() => {
                    this.props.navigation.navigate('MyAccount');
                    this.props.navigation.closeDrawer();
                }}>
                    <Icon name='account-circle' type='material' />
                    <Text style = {styles.rowText}> My Account</Text>
                </TouchableOpacity>
                <TouchableOpacity style = {styles.evenRow} onPress={() => {
                    this.onLogoutClick();
                }}>
                    <Icon name='https' type='material' />
                    <Text style = {styles.rowText}> Logout</Text>
                </TouchableOpacity>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    evenRow: {
        padding: 10,
        height: 60,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
    },
    oddRow: {
        padding: 10,
        height: 60,
        backgroundColor: '#ededed',
        flexDirection: 'row',
        alignItems: 'center'
    },
    rowText: {
        fontWeight: 'bold'
    }
});
