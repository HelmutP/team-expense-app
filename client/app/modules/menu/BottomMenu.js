import AbstractComponent from '../../screens/AbstractComponent';
import React from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';
import BottomNavigation, {
    IconTab,
    Badge
} from 'react-native-material-bottom-navigation';
import { Icon } from 'react-native-elements';

export default class BottomMenu extends AbstractComponent {

    constructor(props) {
        super(props);

        this.tabs = [
            {
                key: 'share',
                label: 'ShareTrip',
                barColor: '#000',
                pressColor: 'rgba(255, 255, 255, 0.16)',
                icon: 'share'
            },
            {
                key: 'balance',
                label: 'TripBalance',
                barColor: '#000',
                pressColor: 'rgba(255, 255, 255, 0.16)',
                icon: 'insert-chart'
            },
            {
                key: 'transactions',
                label: 'Transactions',
                barColor: '#000',
                pressColor: 'rgba(255, 255, 255, 0.16)',
                icon: 'swap-vert'
            },
            {
                key: 'add-transaction',
                label: 'AddTransaction',
                barColor: '#000',
                pressColor: 'rgba(255, 255, 255, 0.16)',
                icon: 'library-add'
            }
        ];
    }

    renderIcon = icon => () => (
        <Icon size={28} color="white" name={icon} />
    )

    renderTab = ({ tab }) => (
        <IconTab isActive={true} key={tab.key} label={tab.label} renderIcon={this.renderIcon(tab.icon)}/>
    )

    onTabPress(newTab) {
        this.props.navigation.navigate(newTab.label);
    }

    render() {
        return (
            <View style={styles.bottomMenuContainer}>
                <BottomNavigation tabs={this.tabs} onTabPress={newTab => this.onTabPress(newTab)} renderTab={this.renderTab}
                    useLayoutAnimation
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    bottomMenuContainer : {
        width: '100%',
        flex: 0.12,
        justifyContent: 'flex-end'
    }
});

