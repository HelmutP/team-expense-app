import React, { Component } from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';

import Login from '../screens/authentication/Login';
import Register from '../screens/authentication/Register';
import Dashboard from '../screens/dashboard/Dashboard';
import MyAccount from '../screens/account/MyAccount';
import SwitchTrip from '../screens/trip/SwitchTrip';
import BurgerMenu from '../modules/menu/BurgerMenu';
import TripSettings from '../screens/trip/TripSettings';
import AddTransaction from '../screens/transactions/AddTransaction';
import Transactions from '../screens/transactions/Transactions';
import ShareTrip from '../screens/trip/ShareTrip';
import TripBalance from '../screens/trip/TripBalance';
import AddTrip from '../screens/trip/AddTrip';

const Drawer = createDrawerNavigator();

export default class RootNavigator extends Component {

    render() {
        return (
            <NavigationContainer>
                <Drawer.Navigator drawerPosition ="right" drawerContent={(navigation) => <BurgerMenu navigation={navigation.navigation} />}>
                    <Drawer.Screen name="Login" component={Login} />
                    <Drawer.Screen name="Dashboard" component={Dashboard} />
                    <Drawer.Screen name="Register" component={Register} />
                    <Drawer.Screen name="MyAccount" component={MyAccount} />
                    <Drawer.Screen name="SwitchTrip" component={SwitchTrip} />
                    <Drawer.Screen name="TripSettings" component={TripSettings} />
                    <Drawer.Screen name="AddTransaction" component={AddTransaction} />
                    <Drawer.Screen name="Transactions" component={Transactions} />
                    <Drawer.Screen name="TripBalance" component={TripBalance} />
                    <Drawer.Screen name="ShareTrip" component={ShareTrip} />
                    <Drawer.Screen name="AddTrip" component={AddTrip} />
                </Drawer.Navigator>
            </NavigationContainer>
        );
    }
}
