import React, { Component } from 'react';
import { Platform, Text, View, AppRegistry, StyleSheet, ScrollView, TouchableOpacity, TextInput } from 'react-native';
import HeaderBar from '../../modules/header/HeaderBar';
import RestrictedAccessScreen from '../RestrictedAccessScreen';
import BottomMenu from '../../modules/menu/BottomMenu';
import { RESTRICTED_ACCESS_STYLES as RESTRICTED_ACCESS_STYLES } from '../../styles/RestrictedAccessStyles';
import TripRestApiDispatcher from '../../api/TripRestApiDispatcher';
import { Icon } from 'react-native-elements';
import { Button } from 'react-native-elements';
import * as Constants from '../../constants/Constants';
import * as BaseUtils from '../../utils/BaseUtils';

const COMPLEX_INPUT_DELIM = ',';
const ATTRIBUTE_FOR_CURRENCY_RATE = 1;
const ATTRIBUTE_FOR_CURRENCY_NAME = 0;
const OWN_TYPE_OF_CURRENCY_ID = 0;

export default class AddTrip extends RestrictedAccessScreen {

    constructor(props) {
        super(props);

        this.initialState = {

            name: '',
            description: '',
            participants: '',
            categories: '',
            currencies: [],

            form: {
                currenciesInputs: [],
                nameError: false,
                descriptionError: false,
                participantsError: false,
                categoriesError: false,
                currenciesError: false
            }
        }

        this.state = {
            ...this.initialState
        }

        this.nameInput = React.createRef();
        this.descriptionInput = React.createRef();
        this.participantsInput = React.createRef();
        this.categoriesInput = React.createRef();
    }

    componentDidMount() {
        this.addCurrencyInput(0);
    }

    createCurrencyInput(key) {

        let updatedCurrencies = this.state.currencies;
        updatedCurrencies.push(['', '']);
        this.setState({...this.state, currencies: updatedCurrencies});

        return (
            <View style={RESTRICTED_ACCESS_STYLES.inputRowBox} key={key+"-currency"}>
                <TextInput style= {{...RESTRICTED_ACCESS_STYLES.input, width: '50%'}} onChangeText = {(text) => {
                    this.handleCurrencyChange(key, ATTRIBUTE_FOR_CURRENCY_NAME, text);
                 }} placeholder = "Currency name" key={key+"-currency-name"} />
                <TextInput style= {{...RESTRICTED_ACCESS_STYLES.input, width: '50%'}} onChangeText = {(text) => {
                    this.handleCurrencyChange(key, ATTRIBUTE_FOR_CURRENCY_RATE, text);
                }} placeholder = "How much is 1 EUR?" key={key+"-currency-ratio"} />
            </View>
        );
    }

    handleCurrencyChange(index, attribute, text) {
        let updatedCurrencies = this.state.currencies;
        updatedCurrencies[index][attribute] = text.trim();
        if (attribute === ATTRIBUTE_FOR_CURRENCY_RATE) {
            updatedCurrencies[index][attribute] = updatedCurrencies[index][attribute].replace(',', '.');
        }
        this.setState({...this.state, currencies: updatedCurrencies});
    }

    addCurrencyInput(key) {
        let currenciesInputs = this.state.form.currenciesInputs;
        currenciesInputs.push(this.createCurrencyInput(key));
        this.setState({...this.state, form: {...this.state.form, currenciesInputs: currenciesInputs}})
    }

    clearForm() {
        this.setState(this.initialState);
        this.addCurrencyInput(0);

        this.nameInput.clear();
        this.descriptionInput.clear();
        this.participantsInput.clear();
        this.categoriesInput.clear();
    }

    onFormSubmit() {
        if (this.isFormValid()) {

            let participantsList = this.processComplexInput(this.state.participants);
            let categoriesList = this.processComplexInput(this.state.categories);
            let currenciesList = this.processCurrenciesInputs(this.state.currencies);

            TripRestApiDispatcher.createTrip(
                Constants.REST_API_BASE_URL + Constants.REST_API_CREATE_TRIP_URL + "/" + this.store.getState().user.id,
                { name: this.state.name, description: this.state.description, participants: participantsList,
                    transactionCategories: categoriesList, currencies: currenciesList }
            )
            .then(response => response.json())
            .then(createdTrip => {
                this.clearForm();

                if (createdTrip !== undefined) {
                    TripRestApiDispatcher.setCurrentTripForUser(
                        Constants.REST_API_BASE_URL + Constants.REST_API_SET_CURRENT_TRIP_URL,
                        this.store.getState().user.id, createdTrip.id
                    )
                    .then(this.props.navigation.navigate('Dashboard'));
                }
            });
        }
    }

    processCurrenciesInputs(currencies) {
        let currenciesObjects = [];
        currencies.forEach(currency => {
            if (currency[ATTRIBUTE_FOR_CURRENCY_NAME] !== undefined && currency[ATTRIBUTE_FOR_CURRENCY_NAME].length > 0 &&
                currency[ATTRIBUTE_FOR_CURRENCY_RATE] !== undefined && currency[ATTRIBUTE_FOR_CURRENCY_RATE].length > 0) {

                currenciesObjects.push({
                    type: OWN_TYPE_OF_CURRENCY_ID,
                    name: currency[ATTRIBUTE_FOR_CURRENCY_NAME],
                    rate: currency[ATTRIBUTE_FOR_CURRENCY_RATE]
                });
            }
        });
        return currenciesObjects;
    }

    processComplexInput(inputState) {

        let objectsList = [];
        let objectsNames = [];

        if (inputState.includes(COMPLEX_INPUT_DELIM)) {
            objectsNames = inputState.split(COMPLEX_INPUT_DELIM);
            objectsNames.forEach(name => {
                if (name !== undefined && name !== null) {
                    let trimmedName = name.trim();
                    if (trimmedName.length > 0) {
                        objectsList.push({name: name.trim()});
                    }
                }
            });
        } else {
            objectsList.push({name: inputState.trim()});
        }
        return objectsList;
    }

    isFormValid() {

        let isNameValid = true;
        let isDescriptionValid = true;
        let isParticipantsValid = true;
        let areCurrenciesValid = true;

        if (this.state.name === undefined || this.state.name.length === 0) {
            isNameValid = false;
        }
        if (this.state.description === undefined || this.state.description.length === 0) {
            isDescriptionValid = false;
        }
        if (this.state.participants === undefined || this.state.participants.length === 0) {
            isParticipantsValid = false;
        }

        this.state.currencies.forEach(currency => {

            let currencyName = currency[ATTRIBUTE_FOR_CURRENCY_NAME];
            let currencyRate = currency[ATTRIBUTE_FOR_CURRENCY_RATE];

            if (currencyName.length > 0 || currencyRate.length > 0) {
                areCurrenciesValid = areCurrenciesValid && BaseUtils.isNumber(currencyRate)
            }
        });

        console.log(this.state.currencies);

        this.setState({...this.state, form: { ...this.state.form, descriptionError: !isDescriptionValid, nameError: !isNameValid,
            participantsError: !isParticipantsValid, currenciesError: !areCurrenciesValid}
        });

        return isNameValid && isDescriptionValid && isParticipantsValid && areCurrenciesValid;
    }

    render() {
        return (
            <View style={RESTRICTED_ACCESS_STYLES.container}>
                <HeaderBar navigation={this.props.navigation} />

                <ScrollView style={RESTRICTED_ACCESS_STYLES.contentContainer}>

                    <View style={RESTRICTED_ACCESS_STYLES.titleBox}>
                        <Text style={RESTRICTED_ACCESS_STYLES.title}>Add new trip</Text>
                    </View>

                    <TextInput
                        style= {{...RESTRICTED_ACCESS_STYLES.input, borderColor: this.state.form.nameError ? 'red' : '#e6e6e6'}}
                        onChangeText = {(text) => {
                            this.setState({...this.state, name: text, form: {...this.state.form, nameError: false}});
                        }}
                        ref={component => this.nameInput = component}
                        placeholder = "Enter name" />

                    <TextInput
                        style= {{...RESTRICTED_ACCESS_STYLES.textArea, borderColor: this.state.form.descriptionError ? 'red' : '#e6e6e6'}}
                        onChangeText = {(text) => {
                            this.setState({...this.state, description: text, form: {...this.state.form, descriptionError: false}});
                        }}
                        ref={component => this.descriptionInput = component}
                        placeholder = "Enter description"
                        multiline={true}
                        numberOfLines={3} />

                    <TextInput
                        style= {{...RESTRICTED_ACCESS_STYLES.textArea, borderColor: this.state.form.participantsError ? 'red' : '#e6e6e6'}}
                        onChangeText = {(text) => {
                            this.setState({...this.state, participants: text, form: {...this.state.form, participantsError: false}});
                        }}
                        ref={component => this.participantsInput = component}
                        placeholder = "Enter participants delimited by comma"
                        multiline={true}
                        numberOfLines={2} />

                    <TextInput
                        style= {{...RESTRICTED_ACCESS_STYLES.textArea, borderColor: this.state.form.categoriesError ? 'red' : '#e6e6e6'}}
                        onChangeText = {(text) => {
                            this.setState({...this.state, categories: text, form: {...this.state.form, categoriesError: false}});
                        }}
                        ref={component => this.categoriesInput = component}
                        placeholder = "Enter transaction categories delimited by comma"
                        multiline={true}
                        numberOfLines={2} />

                    <View style={{borderColor: 'red', borderWidth: this.state.form.currenciesError ? 1 : 0}}>
                        <Text style={RESTRICTED_ACCESS_STYLES.subtitle}>Add custom currencies</Text>

                        {this.state.form.currenciesInputs.map((value, index) => {
                            return value
                        })}
                    </View>

                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'flex-end'}}>
                        <Button raised={true} type='solid' title='Add another currency'
                            onPress={() => this.addCurrencyInput(this.state.form.currenciesInputs.length)}
                            buttonStyle={{...RESTRICTED_ACCESS_STYLES.button}}
                            containerStyle={{...RESTRICTED_ACCESS_STYLES.buttonContainer, height: 40, width: 180}}/>
                    </View>

                    <Button title="Add trip" onPress={() => this.onFormSubmit()} raised={true} type='solid'
                        buttonStyle={{backgroundColor: 'green', height: 50}}
                        containerStyle={{...RESTRICTED_ACCESS_STYLES.buttonContainer, marginBottom: 20}}/>
                </ScrollView>

                <BottomMenu navigation={this.props.navigation} />
            </View>
        );
    }
}

AppRegistry.registerComponent('AddTrip', () => AddTrip);
