import React, { Component } from 'react';
import { Platform, Text, View, AppRegistry, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import HeaderBar from '../../modules/header/HeaderBar';
import RestrictedAccessScreen from '../RestrictedAccessScreen';
import BottomMenu from '../../modules/menu/BottomMenu';
import { RESTRICTED_ACCESS_STYLES as RESTRICTED_ACCESS_STYLES } from '../../styles/RestrictedAccessStyles';
import { Icon } from 'react-native-elements';
import { Button } from 'react-native-elements';
import TripRestApiDispatcher from '../../api/TripRestApiDispatcher';
import * as Constants from '../../constants/Constants';

export default class SwitchTrip extends RestrictedAccessScreen {

    constructor(props) {
        super(props);

        this.state = {
            userTrips: []
        }

        this.subscriptions.push(
            this.store.subscribe(() => {
                this.loadTrips();
            })
        );
        this.loadTrips();
    }
    
    loadTrips() {
        TripRestApiDispatcher.getRequest(Constants.REST_API_BASE_URL + Constants.REST_API_TRIPS_OF_USER_URL + "/" + this.store.getState().user.id)
        .then((trips) => {
            let tripIdsAndNames = this.mapTrips(trips);
            this.setState({userTrips: tripIdsAndNames})
        });
    }

    mapTrips(trips) {
        let tripIdsAndNames = [];
        if (trips !== undefined && trips !== null && trips.length > 0) {
            trips.forEach(trip => tripIdsAndNames.push({id: trip.id, name: trip.name}));
        }
        return tripIdsAndNames;
    }
    
    onSwitchTripClick(trip) {
        TripRestApiDispatcher.setCurrentTripForUser(
            Constants.REST_API_BASE_URL + Constants.REST_API_SET_CURRENT_TRIP_URL,
            this.store.getState().user.id, trip.id, this.store)
        .then(this.props.navigation.navigate('Dashboard'));
    } 
    
    render() {

        let userTripsListBlock;

        if (this.state.userTrips.length === 0) {
            userTripsListBlock =
            <View style = {styles.oddRow}>
                <Text style = {styles.rowText}> No trips found!</Text>
            </View>
        } else {
            userTripsListBlock = this.state.userTrips.map((trip, index) => (
                <TouchableOpacity style = {index % 2 == 0 ? styles.evenRow : styles.oddRow} key={trip.id} onPress={() => {this.onSwitchTripClick(trip)}}>
                    <Text style = {styles.rowText}> {trip.name}</Text>
                </TouchableOpacity>
            ));
        }

        return (
            <View style={RESTRICTED_ACCESS_STYLES.container}>
                <HeaderBar navigation={this.props.navigation} />

                <ScrollView style={RESTRICTED_ACCESS_STYLES.contentContainer}>
                    <View style={RESTRICTED_ACCESS_STYLES.titleBox}>
                        <Text style={RESTRICTED_ACCESS_STYLES.title}>SwitchTrip</Text>
                        <Button buttonStyle={RESTRICTED_ACCESS_STYLES.addButton} icon={{ name: "add-circle", size: 15, color: "white"}}
                            title="Add new trip" onPress={() => this.props.navigation.navigate('AddTrip')}
                        />
                    </View>
                    {userTripsListBlock}
                </ScrollView>
                <BottomMenu navigation={this.props.navigation} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    evenRow: {
        padding: 10,
        height: 40,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
    },
    oddRow: {
        padding: 10,
        height: 40,
        backgroundColor: '#ededed',
        flexDirection: 'row',
        alignItems: 'center'
    },
    rowText: {
        fontWeight: 'bold'
    }
});


AppRegistry.registerComponent('SwitchTrip', () => SwitchTrip);
