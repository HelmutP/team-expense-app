import React, { Component } from 'react';
import { View } from 'react-native';
import store from '../state/Store';

export default class AbstractComponent extends Component {

    constructor(props) {
        super(props);

        this.store = store;
        this.subscriptions = [];
    }

    componentDidMount() {
    }

    componentWillUnmount() {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    render() {
        return (
            <View></View>
        );
    }
}
