import React, { Component, useState } from 'react';
import { Platform, Text, View, AppRegistry, StyleSheet, ScrollView, TouchableOpacity, TextInput, Picker, Keyboard } from 'react-native';
import HeaderBar from '../../modules/header/HeaderBar';
import RestrictedAccessScreen from '../RestrictedAccessScreen';
import BottomMenu from '../../modules/menu/BottomMenu';
import { RESTRICTED_ACCESS_STYLES as RESTRICTED_ACCESS_STYLES } from '../../styles/RestrictedAccessStyles';
import AbstractRestApiDispatcher from '../../api/AbstractRestApiDispatcher';
import { Icon } from 'react-native-elements';
import { Button } from 'react-native-elements';
import * as Constants from '../../constants/Constants';
import * as BaseUtils from '../../utils/BaseUtils';
import store from '../../state/Store';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import * as MathUtils from '../../utils/MathUtils';
import TransactionRestApiDispatcher from '../../api/TransactionRestApiDispatcher';

const PARTICIPANT_AMOUNT_KEY_PREFIX = "partiipant-amount-";

export default class AddTransaction extends RestrictedAccessScreen {

    constructor(props) {
        super(props);

        this.initialState = {

            name: '',
            amount: '',
            currency: '',
            payer: '',
            participantsAmounts: [],
            date: new Date(),
            participants: [],

            form: {
                nameError: false,
                amountError: false,
                dateError: false,
                showDatePicker: false,
                participantsError: false,
                tripCurrenciesInputs: [],
                payerPickerInputs: [],
                participantsInputs: []
            }
        }

        this.state = {
            ...this.initialState
        }

        this.nameInput = React.createRef();
        this.amountInput = React.createRef();
        this.currencyInput = React.createRef();
        this.payerInput = React.createRef();
        this.dateInput = React.createRef();
        this.participantsAmountsInputsRefs = [];

        this.subscriptions.push(
            this.store.subscribe(() => {
                this.initializeForm();
            })
        );
        this.initializeForm();
    }


    initializeForm() {
        this.loadCurrencies();
        this.loadParticipants();
    }

    loadParticipants() {
        AbstractRestApiDispatcher.getRequest(Constants.REST_API_BASE_URL + Constants.REST_API_TRIP_URL + "/" +
            this.store.getState().currentTrip.id + Constants.REST_API_TRIP_PARTICIPANTS_SUFFIX_URL).then((participants) => {

                let defaultParticipantsAmounts = [];
                participants.forEach(p => defaultParticipantsAmounts.push({index: p.id, value: ''}));
                this.setState({...this.state, participants: participants, participantsAmounts: defaultParticipantsAmounts});

                let participantsInputs = this.mapParticipantsToParticipantInputs(participants);
                let payerPickerInputs = this.mapParticipantsToPayerPickerOptionInputs(participants);
                this.setState({...this.state, form: {...this.state.form, participantsInputs: participantsInputs, payerPickerInputs: payerPickerInputs}});
            });
    }

    loadCurrencies() {
        AbstractRestApiDispatcher.getRequest(Constants.REST_API_BASE_URL + Constants.REST_API_TRIP_URL + "/" +
            this.store.getState().currentTrip.id + Constants.REST_API_TRIP_CURRENCIES_SUFFIX_URL)
        .then((currencies) => {
            let currenciesInputs = this.mapCurrenciesToInputs(currencies);
            this.setState({...this.state, form: {...this.state.form, tripCurrenciesInputs: currenciesInputs}});
        });
    }

    mapParticipantsToParticipantInputs(rawParticipants) {
        let participantsInputs = [];
        let participantNum = 0;

        if (rawParticipants !== undefined && rawParticipants !== null && rawParticipants.length > 0) {
            rawParticipants.forEach(participant => {
                participantsInputs.push(
                    (
                        <View style={{...RESTRICTED_ACCESS_STYLES.inputRowBox, flexDirection: 'row', alignItems: 'center'}}
                           ref={component => this.participantsBox = component}>
                        <Text style={{width: '30%', justifyContent: 'center'}}>{participant.name}</Text>
                        <TextInput style= {{...RESTRICTED_ACCESS_STYLES.input, width: '70%', justifyContent: 'center'}}
                            onChangeText = {(text) => this.participantsAmountsChanged(text, participant.id)}
                            value = {this.state.participantsAmounts[participantNum].value} placeholder = "Enter participant share amount"
                            ref={component => this.participantsAmountsInputsRefs.push(component)} />
                        </View>
                    )
                );
                participantNum++;
            });
        }
        return participantsInputs;
    }

    mapParticipantsToPayerPickerOptionInputs(rawParticipants) {
        let pickerOptionInputs = [];
        if (rawParticipants !== undefined && rawParticipants !== null && rawParticipants.length > 0) {
            rawParticipants.forEach(participant => pickerOptionInputs.push(
                (<Picker.Item label={participant.name} value={participant.id} key={participant.id+"-participant-input"} />)
            ));
        }
        return pickerOptionInputs;
    }

    mapCurrenciesToInputs(rawCurrencies) {
        let availableCurrenciesInputs = [];
        if (rawCurrencies !== undefined && rawCurrencies !== null && rawCurrencies.length > 0) {
            rawCurrencies.forEach(currency => availableCurrenciesInputs.push(
                (<Picker.Item label={currency.name} value={currency.id} key={currency.id+"-currency-input"} />)
            ));
        }
        return availableCurrenciesInputs;
    }

    participantsAmountsChanged = (text, index) => {

        let newParticipantsAmounts = this.state.participantsAmounts;
        newParticipantsAmounts.forEach(participantAmount => {
            if (participantAmount.index === index) {
                participantAmount.value = text.replace(',', '.');
            }
        })

        let participantsInputs = this.mapParticipantsToParticipantInputs(this.state.participants);
        this.setState({...this.state, participantsAmounts: newParticipantsAmounts, form: {...this.state.form, participantsInputs: participantsInputs, participantsError: false}});
    }

    showDateTimePicker = () => {
        Keyboard.dismiss();
        this.setState({ ...this.state, form: {...this.state.form, showDatePicker: true }});
    };
    hideDateTimePicker = () => {
        this.setState({ ...this.state, form: {...this.state.form, showDatePicker: false }});
    };
    handleDatePicked = value => {
        this.dateInput.blur();
        this.setState({ ...this.state, date: value, form: {...this.state.form, showDatePicker: false}});
    };

    onAmountChange = (newAmount) => {

        let preprocessedNewAmount = newAmount !== undefined ? newAmount.replace(',', '.') : '';

        if (BaseUtils.isNumber(preprocessedNewAmount)) {

            let newParticipantAmounts = [];
            let newAmountNumber = parseFloat(preprocessedNewAmount);
            let newAmountNumberRounded = MathUtils.roundOnTwoDecimalPlaces(newAmountNumber);
            let oneParticipantAmount = MathUtils.roundOnTwoDecimalPlaces(newAmountNumber / this.state.participantsAmounts.length);
            let lastParticipantAmount = MathUtils.roundOnTwoDecimalPlaces(newAmountNumberRounded - (oneParticipantAmount * (this.state.participantsAmounts.length - 1)));

            newParticipantAmounts = this.state.participantsAmounts;
            for (i = 0; i < newParticipantAmounts.length; i++) {
                if (i == newParticipantAmounts.length - 1) {
                    newParticipantAmounts[i].value = String(lastParticipantAmount);
                } else {
                    newParticipantAmounts[i].value = String(oneParticipantAmount);
                }
            }

            let participantsInputs = this.mapParticipantsToParticipantInputs(this.state.participants);
            this.setState({...this.state, amount: String(newAmountNumberRounded), participantsAmounts: newParticipantAmounts,
                form: {...this.state.form, participantsInputs: participantsInputs, participantsError: false, amountError: false}});
        } else {
            this.setState({...this.state, amount: newAmount, form: {...this.state.form, participantsError: true, amountError: true}});
        }
    }

    onFormSubmit() {

        if (this.isFormValid()) {

            let transactionParticipants = [];
            this.state.participantsAmounts.forEach(pa => transactionParticipants.push({participantId: pa.index, amount: pa.value}));

            TransactionRestApiDispatcher.addTransaction(Constants.REST_API_BASE_URL + Constants.REST_API_TRANSACTION_URL,
                { 
                    name: this.state.name, amount: this.state.amount, currencyId: this.state.currency, payerId: this.state.payer, 
                    date: this.state.date, transactionParticipants: transactionParticipants, tripId: this.store.getState().currentTrip.id
                }
            )
            .then(response => response.json())
            .then(newTransaction => {
                this.clearForm();
                this.props.navigation.navigate('Dashboard');
            });
        }
    }

    isFormValid() {
        let isNameValid = true;
        let isAmountValid = BaseUtils.isNumber(this.state.amount);
        let isDateValid = true;
        let areParticipantsAmountsValid = this.areParticipantsAmountsValid();

        if (this.state.name === undefined || this.state.name.length === 0) {
            isNameValid = false;
        }
        if (this.state.date === undefined || this.state.date.length === 0 || this.state.date == 'Invalid date') {
            isDateValid = false;
        }

        this.setState({...this.state, form: { ...this.state.form, nameError: !isNameValid, amountError: !isAmountValid,
            dateError: !isDateValid, participantsError: !areParticipantsAmountsValid}});

        return isNameValid && isAmountValid && isDateValid && areParticipantsAmountsValid;
    }

    areParticipantsAmountsValid() {
        
        let participantsAmountSum = 0.00;
        this.state.participantsAmounts.forEach(pa => {
            if (pa.value === undefined || pa.value.length == 0 || BaseUtils.isNumber(pa.value) === false) {
                return false;
            }
            participantsAmountSum += Number(pa.value);
        });

        if (participantsAmountSum != this.state.amount) {
            return false;
        }
        return true;
    }

    render() {
        return (
            <View style={RESTRICTED_ACCESS_STYLES.container}>
                <HeaderBar navigation={this.props.navigation} />

                <ScrollView style={RESTRICTED_ACCESS_STYLES.contentContainer}>

                    <View style={RESTRICTED_ACCESS_STYLES.titleBox}>
                        <Text style={RESTRICTED_ACCESS_STYLES.title}>Add transaction</Text>
                    </View>

                    <TextInput
                        style= {{...RESTRICTED_ACCESS_STYLES.input,
                            borderColor: this.state.form.nameError ? Constants.ERROR_INPUT_COLOR : Constants.VALID_INPUT_COLOR}}
                        onChangeText = {(text) => {
                            this.setState({...this.state, name: text, form: {...this.state.form, nameError: false}});
                        }}
                        ref={component => this.nameInput = component}
                        placeholder = "Enter transaction name" />

                    <View style={RESTRICTED_ACCESS_STYLES.inputRowBox}>
                        <TextInput style= {{...RESTRICTED_ACCESS_STYLES.input, width: '50%',
                            borderColor: this.state.form.amountError ? Constants.ERROR_INPUT_COLOR : Constants.VALID_INPUT_COLOR}}
                            placeholder = "Amount"
                            onChangeText = {this.onAmountChange}
                            ref={component => this.amountInput = component}
                        />
                        <Picker style={{...RESTRICTED_ACCESS_STYLES.input, width: '50%',
                            borderColor: this.state.form.currencyError ? Constants.ERROR_INPUT_COLOR : Constants.VALID_INPUT_COLOR}}
                            onValueChange={(value) => this.setState({...this.state, currency: value})} selectedValue={this.state.currency}
                            ref={component => this.currencyInput = component}>

                            {this.state.form.tripCurrenciesInputs.map((value, index) => {
                                return value
                            })}
                        </Picker>
                    </View>

                    <View style={RESTRICTED_ACCESS_STYLES.inputRowBox}>
                        <TextInput
                            style= {{...RESTRICTED_ACCESS_STYLES.input, width: '50%',
                                borderColor: this.state.form.dateError ? Constants.ERROR_INPUT_COLOR : Constants.VALID_INPUT_COLOR}}
                            onChangeText = {(date) => {
                                this.setState({...this.state, date: date, form: {...this.state.form, dateError: false}});
                            }}
                            onFocus={this.showDateTimePicker} value={moment(this.state.date).format('DD/MM/YYYY')}
                            ref={component => this.dateInput = component} placeholder = "Enter transaction date" />

                        <DateTimePicker
                            date={this.state.date}
                            isVisible={this.state.form.showDatePicker}
                            mode={'date'}
                            onConfirm={this.handleDatePicked}
                            onCancel={this.hideDateTimePicker}
                        />

                        <Picker style={{...RESTRICTED_ACCESS_STYLES.input, width: '50%',
                            borderColor: this.state.form.payerError ? Constants.ERROR_INPUT_COLOR : Constants.VALID_INPUT_COLOR}}
                            onValueChange={(value) => this.setState({...this.state, payer: value})} selectedValue={this.state.payer}
                            ref={component => this.payerInput = component} >

                            {this.state.form.payerPickerInputs.map((value) => {
                                return value
                            })}
                        </Picker>
                    </View>

                    <View style={{padding: 3, borderWidth: 1, borderColor: this.state.form.participantsError ? Constants.ERROR_INPUT_COLOR : Constants.VALID_INPUT_COLOR}}>
                        <Text style={{...RESTRICTED_ACCESS_STYLES.boldText}}>Participants</Text>
                        {this.state.form.participantsInputs.map((value) => {
                            return value
                        })}
                    </View>

                    <Button title="Add transaction" onPress={() => this.onFormSubmit()} raised={true} type='solid'
                        buttonStyle={{backgroundColor: 'green', height: 50}}
                        containerStyle={{...RESTRICTED_ACCESS_STYLES.buttonContainer, marginBottom: 20}} />
                </ScrollView>

                <BottomMenu navigation={this.props.navigation} />
            </View>
        );
    }

    clearForm() {
        this.setState(this.initialState);

        this.nameInput.clear();
        this.amountInput.clear();
        this.currencyInput.value = null;
        this.payerInput.value = null;
        this.state.date = new Date();

        this.initializeForm();
    }
}

AppRegistry.registerComponent('AddTransaction', () => AddTransaction);
