import React, { Component } from 'react';
import { Platform, Text, View, Button, AppRegistry, StyleSheet, TextInput } from 'react-native';
import HeaderBar from '../../modules/header/HeaderBar';
import RestrictedAccessScreen from '../RestrictedAccessScreen';
import BottomMenu from '../../modules/menu/BottomMenu';
import { RESTRICTED_ACCESS_STYLES as RESTRICTED_ACCESS_STYLES } from '../../styles/RestrictedAccessStyles';

export default class Transactions extends RestrictedAccessScreen {
    
    constructor(props) {
        super(props);
    }
    
    render() {
        return (
            <View style={RESTRICTED_ACCESS_STYLES.container}>
                <HeaderBar navigation={this.props.navigation} />
                <View style={RESTRICTED_ACCESS_STYLES.contentContainer}>
                    <Text>Transactions</Text>
                    <Text>Transactions</Text>
                    <Text>Transactions</Text>
                    <Text>Transactions</Text>
                    <Text>Transactions</Text>
                    <Text>Transactions</Text>
                    <Text>Transactions</Text>
                    <Text>Transactions</Text>
                </View>
                <BottomMenu navigation={this.props.navigation} />
            </View>
        );
    }
}

AppRegistry.registerComponent('Transactions', () => Transactions);
