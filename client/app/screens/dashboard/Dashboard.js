import React, { Component } from 'react';
import { Platform, Text, View, AppRegistry, Button } from 'react-native';
import HeaderBar from '../../modules/header/HeaderBar';
import * as Constants from '../../constants/Constants';
import store from '../../state/Store';
import RestrictedAccessScreen from '../RestrictedAccessScreen';
import TripRestApiDispatcher from '../../api/TripRestApiDispatcher';
import BottomMenu from '../../modules/menu/BottomMenu';
import { RESTRICTED_ACCESS_STYLES as RESTRICTED_ACCESS_STYLES } from '../../styles/RestrictedAccessStyles';

export default class Dashboard extends RestrictedAccessScreen {

    constructor(props) {
        super(props);

        this.state = {
            currentTrip: undefined
        }

        this.subscriptions.push(
            this.store.subscribe(() => {
                let currentState = this.store.getState();

                if (currentState.currentTrip === undefined) {
                    this.setState({currentTrip: undefined});
                } else {
                    this.setState({currentTrip: currentState.currentTrip});
                }

                /*if ((currentState.currentTrip === undefined && currentState.user.currentTripId !== undefined) ||
                    (currentState.currentTrip !== undefined && currentState.user.currentTripId !== undefined &&
                        currentState.currentTrip.id !== currentState.user.currentTripId)) {
                    this.getCurrentTrip();
                }*/
            })
        );
    }

    getCurrentTrip() {
        TripRestApiDispatcher.getCurrentTrip(
            Constants.REST_API_BASE_URL + Constants.REST_API_TRIP_URL + "/" + this.store.getState().user.currentTripId, this.store);
    }

    render() {

        let screenContent;
        if (this.state.currentTrip !== undefined) {
            screenContent = <Text>{this.state.currentTrip.name}</Text>;
        } else {
            screenContent = <Text>No trip</Text>;
        }

        return (
            <View style={RESTRICTED_ACCESS_STYLES.container}>
                <HeaderBar navigation={this.props.navigation} />
                <View style={RESTRICTED_ACCESS_STYLES.contentContainer}>
                    {screenContent}
                </View>
                <BottomMenu navigation={this.props.navigation} />
            </View>
        );
    }
}

AppRegistry.registerComponent('Dashboard', () => Dashboard);
