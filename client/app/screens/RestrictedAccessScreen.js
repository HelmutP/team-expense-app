import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import store from '../state/Store';
import AuthenticationGuard from '../authentication/AuthenticationGuard';
import AbstractComponent from './AbstractComponent';

export default class RestrictedAccessScreen extends AbstractComponent {

    constructor(props) {
        super(props);

        this.subscriptions.push(
            this.store.subscribe(() => {
                AuthenticationGuard.checkUserLoggedIn(this.props.navigation);
            })
        );
        AuthenticationGuard.checkUserLoggedIn(this.props.navigation);
    }

    render() {
        return (
            <View></View>
        );
    }
}
