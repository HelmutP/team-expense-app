import React, { Component } from 'react';
import { Platform, Text, View, AppRegistry, StyleSheet, TextInput } from 'react-native';
import AuthenticationGuard from '../../authentication/AuthenticationGuard';
import SnackBar from 'react-native-snackbar-component';
import * as Constants from '../../constants/Constants';
import { AUTH_STYLES as AUTH_STYLES } from '../../styles/Styles';
import AuthenticationRestApiDispatcher from '../../api/AuthenticationRestApiDispatcher';
import AbstractComponent from '../AbstractComponent';
import { Icon } from 'react-native-elements';
import { Button } from 'react-native-elements';

export default class Register extends AbstractComponent {

    constructor(props) {
        super(props)

        this.state = {
            username: '',
            password: '',
            iban: '',
            displayName: '',
            snackBar: {
                visible: false,
                text: '',
                actionText: ''
            }
        }
    }

    onRegisterSubmitted() {

        if (this.validateFormValues()) {
            AuthenticationRestApiDispatcher.register(Constants.REST_API_BASE_URL + Constants.REST_API_REGISTER_URL, this.store, this,
                this.state.username, this.state.password, this.state.iban, this.state.displayName);
        } else {
            this.setState({snackBar: {visible: true, text: 'Username, Password and Name are mandatory.', actionText: 'OK'}});
        }
    }

    validateFormValues() {
        if (this.state.username !== undefined && this.state.username.length > 0 && this.state.password !== undefined &&
            this.state.password.length > 0 && this.state.displayName !== undefined && this.state.displayName.length > 0) {
            return true;
        }
        return false;
    }

    render() {
        return (
            <View style={AUTH_STYLES.authContainer}>
                <Icon size={28} color="black" name="airplanemode-active" />
                <Text style={AUTH_STYLES.header}>Registration</Text>

                <TextInput style = {AUTH_STYLES.loginInput}
                    onChangeText = {(text) => this.setState({username:text})} placeholder = "Enter username" />
                <TextInput style = {AUTH_STYLES.loginInput} onChangeText = {(text) => this.setState({password:text})}
                    secureTextEntry={true} placeholder = "Enter password" />
                <TextInput style = {AUTH_STYLES.loginInput}
                    onChangeText = {(text) => this.setState({displayName:text})} placeholder = "Enter your name" />
                <TextInput style = {AUTH_STYLES.loginInput}
                    onChangeText = {(text) => this.setState({iban:text})} placeholder = "Enter IBAN" />
                <Button style = {AUTH_STYLES.registerButton} title="Register" onPress={() => this.onRegisterSubmitted()}/>

                <SnackBar visible={this.state.snackBar.visible} textMessage={this.state.snackBar.text}
                    actionHandler={()=>{
                        this.state.snackBar.actionText === 'Login' ? this.props.navigation.navigate('Login') :
                            this.setState({snackBar: {visible: false, text: ''}, actionText: ''});}}
                    actionText={this.state.snackBar.actionText} />
            </View>
        );
    }
}

AppRegistry.registerComponent('Register', () => Register);
