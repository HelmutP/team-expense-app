import React, { Component } from 'react';
import { Platform, Text, View, AppRegistry, StyleSheet, TextInput, Alert, Modal, TouchableHighlight  } from 'react-native';
import AuthenticationGuard from '../../authentication/AuthenticationGuard';
import SnackBar from 'react-native-snackbar-component'
import * as Constants from '../../constants/Constants';
import { AUTH_STYLES as AUTH_STYLES } from '../../styles/Styles';
import store from '../../state/Store';
import AbstractComponent from '../AbstractComponent';
import AuthenticationRestApiDispatcher from '../../api/AuthenticationRestApiDispatcher';
import { Icon } from 'react-native-elements';
import { Button } from 'react-native-elements';

export default class Login extends AbstractComponent {

    constructor(props) {
        super(props);

        this.initialState = {
            username: '',
            password: '',
            snackBar: {
                visible: false,
                text: ''
            }
        }
        this.state = {
            ...this.initialState
        }

        this.passwordInput = React.createRef();
        this.usernameInput = React.createRef();

        this.redirectIfLoggedIn();
    }

    onLoginSubmitted() {
        AuthenticationRestApiDispatcher.login(
            Constants.REST_API_BASE_URL + Constants.REST_API_LOGIN_URL, store, this, this.state.username, this.state.password);
    }

    clearForm() {
        this.passwordInput.clear();
        this.usernameInput.clear();
    }

    redirectIfLoggedIn() {
        if (this.store.getState().user.isLoggedIn) {
            this.props.navigation.navigate('Dashboard');
        }
    }

    render() {
        return (
                <View style={AUTH_STYLES.authContainer}>
                    <Icon size={28} color="black" name="flight-takeoff" />
                    <Text style={AUTH_STYLES.welcomeTitle}>Welcome to TeamExpense!</Text>

                    <TextInput
                        style = {AUTH_STYLES.loginInput}
                        onChangeText = {(text) => this.state.username = text}
                        ref={component => this.usernameInput = component}
                        placeholder = "Enter username" />
                            
                    <TextInput
                        style = {AUTH_STYLES.loginInput}
                        onChangeText = {(text) => this.state.password = text}
                        secureTextEntry={true}
                        ref={component => this.passwordInput = component}
                        placeholder = "Enter password" />
                    <Button title="Login" onPress={() => this.onLoginSubmitted()} raised={true} type='solid' titleStyle={AUTH_STYLES.button}/>

                    <Text style = {AUTH_STYLES.noAccountTitle}>No account yet?</Text>
                    <Button title="Register" onPress={() => this.props.navigation.navigate('Register')} titleStyle={AUTH_STYLES.button} />

                    <SnackBar visible={this.state.snackBar.visible} textMessage={this.state.snackBar.text}
                        actionHandler={()=>{this.setState({snackBar: {visible: false, text: ''}})}}
                        actionText="OK" />
                </View>
        );
    }
}

AppRegistry.registerComponent('Login', () => Login);
