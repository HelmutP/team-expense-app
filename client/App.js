import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Navigator, AppRegistry } from 'react-native';
import RootNavigator from './app/navigation/RootNavigator';

export default class App extends Component {

  render() {
    return (
        <RootNavigator />
    );
  }
}
AppRegistry.registerComponent('App', () => App);
