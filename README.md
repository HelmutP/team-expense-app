# Team Expense App

App is supposed to share expenses with your friends on any event you attend together, focused on travel.

Less stress, less time spent on administration stuff and more time enjoyed, that should be goal of the app.

## Development

I develop this app during my free time step by step. If you would like to contribute, just let me know.

## Technologies

Java 8, Spring Boot, MySQL, React Native (for now).

 


