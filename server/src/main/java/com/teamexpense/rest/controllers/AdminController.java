package com.teamexpense.rest.controllers;

import com.teamexpense.rest.constants.RestApiConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional
public class AdminController extends AbstractController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);

    public AdminController() {
        LOGGER.debug("AdminController created.");
    }

    @GetMapping(value={RestApiConst.REST_API_URL_ADMIN_ALIVE}, produces=RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<String> info() {
        LOGGER.info("GET request for info catched.");
        return ResponseEntity.ok().body("I am alive");
    }
}
