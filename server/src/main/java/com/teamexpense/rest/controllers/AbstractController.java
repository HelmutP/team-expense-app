package com.teamexpense.rest.controllers;

import com.teamexpense.rest.constants.RestApiConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(RestApiConst.REST_API_URL_PREFIX)
public abstract class AbstractController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractController.class);

    @ExceptionHandler
    public ResponseEntity<Object> handleException(Exception exception) {
        LOGGER.error("Error occurred during request processing.");
        LOGGER.error(exception.getMessage(), exception);
        return ResponseEntity.badRequest().body(500);
    }
}
