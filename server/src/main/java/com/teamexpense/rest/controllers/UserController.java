package com.teamexpense.rest.controllers;

import com.teamexpense.rest.communication.requests.LoginUserRequest;
import com.teamexpense.rest.communication.requests.RegisterUserRequest;
import com.teamexpense.rest.constants.RestApiConst;
import com.teamexpense.rest.dtos.database.UserDto;
import com.teamexpense.rest.entities.User;
import com.teamexpense.rest.exceptions.ResourceNotFoundException;
import com.teamexpense.rest.exceptions.UserNotFoundException;
import com.teamexpense.rest.mappers.UserMapper;
import com.teamexpense.rest.services.TripService;
import com.teamexpense.rest.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.management.InstanceAlreadyExistsException;
import java.security.NoSuchAlgorithmException;

@RestController
@Transactional
public class UserController extends AbstractController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    private final UserService userService;
    private final UserMapper userMapper;
    private final TripService tripService;

    public UserController(UserService userService, UserMapper userMapper, TripService tripService) {
        LOGGER.debug("UserController created.");

        this.userMapper = userMapper;
        this.userService = userService;
        this.tripService = tripService;
    }

    @GetMapping(value={RestApiConst.REST_API_URL_USER + "/{id}"}, produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<UserDto> getUserById(@PathVariable(value = "id") Long userId) throws ResourceNotFoundException {

        LOGGER.info("GET request for user with id " + userId + " catched.");

        final User user = this.userService.getById(userId);

        LOGGER.info("GET request for user with id " + userId + " processed.");
        return ResponseEntity.ok().body(userMapper.entityToDto(user));
    }

    @GetMapping(value={RestApiConst.REST_API_URL_SET_USER_ACTIVE_TRIP + "/{userId}/{tripId}"}, produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<UserDto> setTripAsActiveForUser(
            @PathVariable(value = "tripId") Long tripId, @PathVariable(value = "userId") Long userId) throws ResourceNotFoundException {

        LOGGER.info("GET request for setting active trip with id " + tripId + " for user with id " + userId + " catched.");

        final User user = this.userService.setActiveTripForUser(this.userService.getById(userId), this.tripService.getById(tripId));

        LOGGER.info("GET request for setting active trip with id " + tripId + " for user with id " + userId + " processed.");
        return ResponseEntity.ok().body(userMapper.entityToDto(user));
    }

    @PostMapping(value={RestApiConst.REST_API_URL_USER_LOGIN}, produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<UserDto> login(@RequestBody LoginUserRequest loginUserReq)
            throws UserNotFoundException, NoSuchAlgorithmException {

        LOGGER.info("Login request " + loginUserReq.toString() + " catched.");

        final User user = this.userService.login(loginUserReq);

        LOGGER.info("Login request " + loginUserReq.toString() + " processed.");
        return ResponseEntity.ok().body(userMapper.entityToDto(user));
    }

    @PostMapping(value={RestApiConst.REST_API_URL_USER_REGISTER}, produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<UserDto> register(@RequestBody RegisterUserRequest registerUserReq)
            throws InstanceAlreadyExistsException, NoSuchAlgorithmException {

        LOGGER.info("Registration request " + registerUserReq.toString() + " catched.");

        final User user = this.userService.register(registerUserReq);

        LOGGER.info("Registration request " + registerUserReq.toString() + " processed.");
        return ResponseEntity.ok().body(userMapper.entityToDto(user));
    }
}
