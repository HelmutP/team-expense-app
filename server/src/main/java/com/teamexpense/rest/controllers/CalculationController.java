package com.teamexpense.rest.controllers;

import com.teamexpense.rest.communication.responses.BalanceResponse;
import com.teamexpense.rest.communication.responses.SpentByCategoryResponse;
import com.teamexpense.rest.constants.RestApiConst;
import com.teamexpense.rest.entities.Trip;
import com.teamexpense.rest.exceptions.ExchangeRateNotFoundException;
import com.teamexpense.rest.exceptions.ResourceNotFoundException;
import com.teamexpense.rest.services.TripService;
import com.teamexpense.rest.services.calculation.CalculationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Map;

@RestController
@Transactional
public class CalculationController extends AbstractController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CalculationController.class);

    private final CalculationService calculationService;
    private final TripService tripService;

    public CalculationController(@Qualifier("CalculationServiceGreedyImpl") CalculationService calculationService, TripService tripService) {

        LOGGER.debug("CalculationController created.");

        this.calculationService = calculationService;
        this.tripService = tripService;
    }

    @GetMapping(value={RestApiConst.REST_API_URL_CALCULATION_BALANCE + "/{id}"}, produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<BalanceResponse> balance(@PathVariable(value = "id") Long tripId) throws ExchangeRateNotFoundException, ResourceNotFoundException {

        LOGGER.info("GET request for balance calculation of trip with id " + tripId + " catched.");

        final BalanceResponse response = new BalanceResponse();
        final Trip trip = this.tripService.getById(tripId);

        response.setTotalSpent(this.calculationService.calculateTotals(trip.getTransactions()));
        response.setSummary(this.calculationService.calculateSummary(trip.getTransactions(), trip.getParticipants()));
        response.setSettle(this.calculationService.calculateHowToSettle(response.getSummary()));

        LOGGER.info("GET request for balance calculation of trip with id " + tripId + " processed.");
        return ResponseEntity.ok().body(response);
    }

    @GetMapping(value={RestApiConst.REST_API_URL_CALCULATION_SPENT_BY_CATEGORY + "/{id}"}, produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<SpentByCategoryResponse> spentByCategory(@PathVariable(value = "id") Long tripId) {

        LOGGER.info("GET request for calculation of spent money by transaction category for trip with id " + tripId + " catched.");

        final SpentByCategoryResponse response = new SpentByCategoryResponse();

        final Map<Long, BigDecimal> spentByCategories = this.calculationService.calculateSpentByCategory(tripId);

        response.setSpentByCategory(spentByCategories);

        LOGGER.info("GET request for calculation of spent money by transaction category for trip with id " + tripId + " processed.");
        return ResponseEntity.ok().body(response);
    }
}
