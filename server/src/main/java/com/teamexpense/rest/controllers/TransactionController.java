package com.teamexpense.rest.controllers;

import com.teamexpense.rest.constants.RestApiConst;
import com.teamexpense.rest.dtos.database.TransactionDto;
import com.teamexpense.rest.entities.Transaction;
import com.teamexpense.rest.entities.Trip;
import com.teamexpense.rest.exceptions.ResourceNotFoundException;
import com.teamexpense.rest.mappers.TransactionMapper;
import com.teamexpense.rest.services.TransactionService;
import com.teamexpense.rest.services.TripService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Transactional
public class TransactionController extends AbstractController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionController.class);

    private final TransactionService transactionService;
    private final TripService tripService;
    private final TransactionMapper transactionMapper;

    public TransactionController(TransactionService transactionService, TripService tripService, TransactionMapper transactionMapper) {
        LOGGER.debug("TransactionController created.");

        this.transactionService = transactionService;
        this.tripService = tripService;
        this.transactionMapper = transactionMapper;
    }

    @GetMapping(value={RestApiConst.REST_API_URL_TRANSACTION + "/{id}"}, produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<TransactionDto> getTransactionById(@PathVariable(value = "id") Long transactionId) throws ResourceNotFoundException {

        LOGGER.info("GET request for transaction with id " + transactionId + " catched.");

        final Transaction transaction = this.transactionService.getById(transactionId);

        LOGGER.info("GET request for transaction with id " + transactionId + " processed.");
        return ResponseEntity.ok().body(transactionMapper.entityToDto(transaction));
    }

    @GetMapping(value={RestApiConst.REST_API_URL_TRANSACTION_OF_TRIP + "/{tripId}"}, produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<List<TransactionDto>> getTransactionsOfTrip(@PathVariable(value = "tripId") Long tripId) {
        return this.getTransactionsOfTripOrdered(tripId, "created-at", "desc");
    }

    @GetMapping(value={RestApiConst.REST_API_URL_TRANSACTION_OF_TRIP + "/{tripId}/order/{orderBy}/{orderWay}"},
        produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<List<TransactionDto>> getTransactionsOfTripOrdered(@PathVariable(value = "tripId") Long tripId,
        @PathVariable(value = "orderBy") String orderBy, @PathVariable(value = "orderWay") String orderWay) {

        LOGGER.info("GET request for transactions with trip id " + tripId + " and ordering by " + orderBy +" (" + orderWay + ") catched.");

        final List<Transaction> transactions = this.transactionService.getOrderedTripTransactions(tripId, orderBy, orderWay);

        LOGGER.info("GET request for transactions with trip id " + tripId + " and ordering by " + orderBy + " (" + orderWay + ") processed.");
        return ResponseEntity.ok().body(transactionMapper.entitiesListToDtosList(transactions));
    }

    @GetMapping(value={RestApiConst.REST_API_URL_TRANSACTION_LATEST + "/{tripId}"},
        produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<List<TransactionDto>> getLatestTripTransactions(@PathVariable(value = "tripId") Long tripId) {

        LOGGER.info("GET request for latest transactions (3) of trip with id " + tripId + " catched.");

        final List<Transaction> latestTransactions = this.transactionService.getLatestTransactionsOfTrip(tripId);

        LOGGER.info("GET request for latest transactions (3) of trip with id " + tripId + " processed.");
        return ResponseEntity.ok().body(transactionMapper.entitiesListToDtosList(latestTransactions));
    }

    @PostMapping(value={RestApiConst.REST_API_URL_TRANSACTION_SEARCH + "/{tripId}"}, produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<List<TransactionDto>> getTripTransactionsContainingText(
            @RequestBody String text, @PathVariable(value = "tripId") Long tripId
    ) {

        LOGGER.info("POST request for transactions containing string " + text + " in trip " + tripId + " catched.");

        final List<Transaction> transactionsWithText =
            this.transactionService.getTransactionsOfTripContainingText(text, tripId);

        LOGGER.info("POST request for transactions containing string " + text + " in trip " + tripId + " processed.");
        return ResponseEntity.ok().body(transactionMapper.entitiesListToDtosList(transactionsWithText));
    }

    @PostMapping(value={RestApiConst.REST_API_URL_TRANSACTION}, produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<TransactionDto> addTransaction(@RequestBody TransactionDto transactionDto) throws ResourceNotFoundException {

        LOGGER.info("POST request with trip " + transactionDto.toString() + " catched.");

        final Trip trip = this.tripService.getById(transactionDto.getTripId());
        final Transaction transactionToBeCreated = this.transactionMapper.dtoToEntity(transactionDto, trip);

        LOGGER.info("POST request with trip " + transactionDto.toString() + " processed.");
        return ResponseEntity.ok().body(
                transactionMapper.entityToDto(this.transactionService.create(transactionToBeCreated)));
    }

    @PutMapping(RestApiConst.REST_API_URL_TRANSACTION + "/{id}")
    public ResponseEntity<TransactionDto> updateTransaction(
        @PathVariable(value = "id") Long transactionId, @RequestBody TransactionDto transactionDetails) throws ResourceNotFoundException {

        LOGGER.info("PUT request of transaction " + transactionId + " catched.");

        final Trip trip = this.tripService.getById(transactionDetails.getTripId());
        final Transaction updatedTransaction = this.transactionService.updateAndSave(transactionId, transactionDetails, trip);

        LOGGER.info("PUT request of transaction " + transactionId + " processed.");
        return ResponseEntity.ok(transactionMapper.entityToDto(updatedTransaction));
    }

    @DeleteMapping(value={RestApiConst.REST_API_URL_TRANSACTION + "/{id}"}, produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<Map<String, Boolean>> deleteTransaction(@PathVariable(value = "id") Long transactionId) throws ResourceNotFoundException {

        LOGGER.info("DELETE request for transaction with id " + transactionId + " catched.");

        final boolean deleteSuccessful = this.transactionService.delete(transactionId);

        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", deleteSuccessful);

        LOGGER.info("DELETE request for transaction with id " + transactionId + " processed.");
        return ResponseEntity.ok(response);
    }
}
