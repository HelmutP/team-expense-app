package com.teamexpense.rest.controllers;

import com.teamexpense.rest.constants.RestApiConst;
import com.teamexpense.rest.dtos.database.CurrencyDto;
import com.teamexpense.rest.dtos.database.TripDto;
import com.teamexpense.rest.dtos.database.ParticipantDto;
import com.teamexpense.rest.entities.Currency;
import com.teamexpense.rest.entities.Trip;
import com.teamexpense.rest.entities.Participant;
import com.teamexpense.rest.exceptions.ResourceNotFoundException;
import com.teamexpense.rest.mappers.CurrencyMapper;
import com.teamexpense.rest.mappers.TripMapper;
import com.teamexpense.rest.mappers.ParticipantMapper;
import com.teamexpense.rest.services.TripService;
import com.teamexpense.rest.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Transactional
public class TripController extends AbstractController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TripController.class);

    private final TripService tripService;
    private final UserService userService;
    private final TripMapper tripMapper;
    private final ParticipantMapper participantMapper;
    private final CurrencyMapper currencyMapper;

    public TripController(TripService tripService, TripMapper tripMapper, UserService userService,
                          ParticipantMapper participantMapper, CurrencyMapper currencyMapper) {

        LOGGER.debug("TripController created.");

        this.participantMapper = participantMapper;
        this.currencyMapper = currencyMapper;
        this.tripService = tripService;
        this.tripMapper = tripMapper;
        this.userService = userService;
    }

    @GetMapping(value={RestApiConst.REST_API_URL_TRIP + "/{id}"}, produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<TripDto> getTripById(@PathVariable(value = "id") Long tripId) throws ResourceNotFoundException {

        LOGGER.info("GET request for trip with id " + tripId + " catched.");

        final Trip trip = this.tripService.getById(tripId);

        LOGGER.info("GET request for trip with id " + tripId + " processed.");
        return ResponseEntity.ok().body(tripMapper.entityToDto(trip));
    }

    @GetMapping(value={RestApiConst.REST_API_URL_TRIPS_OF_USER + "/{userId}"}, produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<List<TripDto>> getTripsOfUser(@PathVariable(value = "userId") Long userId) {

        LOGGER.info("GET request for trips list for user with id " + userId + " catched.");

        final List<Trip> tripsOfUser = this.tripService.getTripsOfUser(userId);

        LOGGER.info("GET request for trips list for user with id " + userId + " processed.");
        return ResponseEntity.ok().body(tripMapper.entitiesListToDtosList(tripsOfUser));
    }

    @GetMapping(value={RestApiConst.REST_API_URL_TRIP + "/{tripId}" + RestApiConst.REST_API_URL_CURRENCIES_OF_TRIP}, produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<List<CurrencyDto>> getCurrenciesOfTrip(@PathVariable(value = "tripId") Long tripId) {

        LOGGER.info("GET request for currencies list of trip with id " + tripId + " catched.");

        final List<Currency> currencies = this.tripService.getTripCurrencies(tripId);

        LOGGER.info("GET request for currencies list of trip with id " + tripId + " processed.");
        return ResponseEntity.ok().body(currencyMapper.entitiesListToDtosList(currencies));
    }

    @GetMapping(value={RestApiConst.REST_API_URL_TRIP + "/{tripId}" + RestApiConst.REST_API_URL_PARTICIPANTS_OF_TRIP}, produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<List<ParticipantDto>> getTripParticipants(@PathVariable(value = "tripId") Long tripId) {

        LOGGER.info("GET request for participants list of trip with id " + tripId + " catched.");

        final List<Participant> participants = this.tripService.getTripParticipants(tripId);

        LOGGER.info("GET request for participants list of trip with id " + tripId + " processed.");
        return ResponseEntity.ok().body(participantMapper.entitiesListToDtosList(participants));
    }

    @PostMapping(value={RestApiConst.REST_API_URL_TRIP_CREATE + "/{userId}"}, produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<TripDto> createTrip(@RequestBody TripDto trip, @PathVariable(value = "userId") Long userId) throws ResourceNotFoundException {

        LOGGER.info("POST request with trip " + trip.getName() + " for user with id " + userId + " catched.");

        final Trip tripToBeCreated = this.tripMapper.dtoToEntity(trip);
        final Trip createdTrip = this.tripService.create(tripToBeCreated);

        this.userService.setActiveTripForUser(this.userService.getById(userId), createdTrip);

        LOGGER.info("POST request with trip " + trip.toString() + " for user with id " + userId + " processed.");
        return ResponseEntity.ok(tripMapper.entityToDto(createdTrip));
    }

    @PutMapping(value={RestApiConst.REST_API_URL_TRIP + "/{id}"}, produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<TripDto> updateTrip(@PathVariable(value = "id") Long tripId, @RequestBody TripDto tripDetails) throws ResourceNotFoundException {

        LOGGER.info("PUT request with trip " + tripDetails.getName() + " catched.");

        final Trip updatedTrip = this.tripService.updateAndSave(tripId, tripDetails);

        LOGGER.info("PUT request with trip " + tripDetails.toString() + " processed.");
        return ResponseEntity.ok(tripMapper.entityToDto(updatedTrip));
    }

    @DeleteMapping(value={RestApiConst.REST_API_URL_TRIP + "/{id}"}, produces= RestApiConst.REST_API_RESPONSE_CONTENT_TYPE)
    public ResponseEntity<Map<String, Boolean>> deleteTrip(@PathVariable(value = "id") Long tripId) throws ResourceNotFoundException {

        LOGGER.info("DELETE request for trip with id " + tripId + " catched.");

        final boolean deleteSuccessful = this.tripService.delete(tripId);

        final Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", deleteSuccessful);

        LOGGER.info("DELETE request for trip with id " + tripId + " processed.");
        return ResponseEntity.ok(response);
    }
}
