package com.teamexpense.rest.dtos.database;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TransactionDto extends AbstractDatabaseDto {

    private String name;
    private BigDecimal amount;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate date;

    private String description;
    private List<TransactionParticipantDto> transactionParticipants =
            new ArrayList<TransactionParticipantDto>();
    private long categoryId;
    private long currencyId;
    private long payerId;
    private long tripId;

    public TransactionDto() {
    }

    public TransactionDto(String name, BigDecimal amount, long currencyId, long payerId) {
        this.name = name;
        this.amount = amount;
        this.currencyId = currencyId;
        this.payerId = payerId;
    }

    public TransactionDto(String name, String description, BigDecimal amount,
                          long currencyId, long payerId, LocalDate date,
                          List<TransactionParticipantDto> transactionParticipants, long categoryId) {

        this(name, amount, currencyId, payerId);
        this.setDate(date);
        this.setDescription(description);
        this.setCategoryId(categoryId);

        if (transactionParticipants != null) {
            transactionParticipants.forEach(tp -> {
                this.addTransactionParticipant(tp);
            });
        }
    }

    public void addTransactionParticipant(long participantId, BigDecimal amount) {
        this.transactionParticipants.add(
                new TransactionParticipantDto(amount, participantId, this.getId())
        );
    }

    public void addTransactionParticipant(TransactionParticipantDto transactionParticipant) {
        this.transactionParticipants.add(transactionParticipant);
    }


    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public long getPayerId() {
        return payerId;
    }

    public void setPayerId(long payerId) {
        this.payerId = payerId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(long currencyId) {
        this.currencyId = currencyId;
    }

    public long getTripId() {
        return tripId;
    }

    public void setTripId(long tripId) {
        this.tripId = tripId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public List<TransactionParticipantDto> getTransactionParticipants() {
        return transactionParticipants;
    }

    public void setTransactionParticipants(List<TransactionParticipantDto> transactionParticipants) {
        this.transactionParticipants = transactionParticipants;
    }
}
