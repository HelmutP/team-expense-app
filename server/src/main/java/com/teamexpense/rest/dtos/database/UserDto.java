package com.teamexpense.rest.dtos.database;

public class UserDto extends AbstractDatabaseDto {

    private String userName;
    private String iban;
    private String displayName;
    private Long activeTripId;


    public Long getActiveTripId() {
        return activeTripId;
    }

    public void setActiveTripId(Long activeTripId) {
        this.activeTripId = activeTripId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
