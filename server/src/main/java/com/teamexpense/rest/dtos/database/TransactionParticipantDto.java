package com.teamexpense.rest.dtos.database;

import java.math.BigDecimal;

public class TransactionParticipantDto extends AbstractDatabaseDto {

    private BigDecimal amount;
    private long participantId;
    private long transactionId;

    public TransactionParticipantDto() {
    }

    public TransactionParticipantDto(BigDecimal amount, long participantId, long transactionId) {
        this(amount, participantId);
        this.transactionId = transactionId;
    }

    public TransactionParticipantDto(BigDecimal amount, long participantId) {
        this.amount = amount;
        this.participantId = participantId;
    }


    public long getParticipantId() {
        return participantId;
    }

    public void setParticipantId(long participantId) {
        this.participantId = participantId;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
