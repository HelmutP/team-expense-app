package com.teamexpense.rest.dtos.calculation.summary;

import java.math.BigDecimal;
import java.math.MathContext;

public class SummaryParticipantDto {

    private BigDecimal paid;
    private BigDecimal charged;
    private long participantId;

    public SummaryParticipantDto() {
        this.paid = BigDecimal.ZERO;
        this.charged = BigDecimal.ZERO;
    }

    public SummaryParticipantDto(long participantId) {
        this();
        this.participantId = participantId;
    }

    public SummaryParticipantDto copy() {
        final SummaryParticipantDto copyObject = new SummaryParticipantDto(this.participantId);
        copyObject.setCharged(new BigDecimal(this.getCharged().toString()));
        copyObject.setPaid(new BigDecimal(this.getPaid().toString()));
        return copyObject;
    }

    public BigDecimal calculateDifference() {
        return paid.subtract(charged, MathContext.UNLIMITED);
    }

    public void minusPaid(BigDecimal paidToBeSubstracted) {
        this.paid = this.paid.subtract(paidToBeSubstracted);
    }

    public void addPaid(BigDecimal paidToBeAdded) {
        this.paid = this.paid.add(paidToBeAdded);
    }

    // -------------- Getters and setters -------------

    public long getParticipantId() {
        return participantId;
    }

    public void setParticipantId(long participantId) {
        this.participantId = participantId;
    }

    public BigDecimal getPaid() {
        return paid;
    }

    public void setPaid(BigDecimal paid) {
        this.paid = paid;
    }

    public BigDecimal getCharged() {
        return charged;
    }

    public void addCharged(BigDecimal chargedToBeAdded) {
        this.charged = this.charged.add(chargedToBeAdded);
    }

    public void setCharged(BigDecimal charged) {
        this.charged = charged;
    }

}
