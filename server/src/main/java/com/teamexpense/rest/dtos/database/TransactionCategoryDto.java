package com.teamexpense.rest.dtos.database;

public class TransactionCategoryDto extends AbstractDatabaseDto {

    private String name;
    private long tripId;

    public TransactionCategoryDto() {
    }

    public TransactionCategoryDto(String name) {
        this.name = name;
    }

    public TransactionCategoryDto(String name, long tripId) {
        this(name);
        this.tripId = tripId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTripId() {
        return tripId;
    }

    public void setTripId(long tripId) {
        this.tripId = tripId;
    }
}
