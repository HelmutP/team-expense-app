package com.teamexpense.rest.dtos.database;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TripDto extends AbstractDatabaseDto {

    private String name;
    private String description;
    private List<ParticipantDto> participants = new ArrayList<ParticipantDto>();
    private List<TransactionDto> transactions = new ArrayList<TransactionDto>();
    private List<TransactionCategoryDto> transactionCategories = new ArrayList<>();
    private Set<CurrencyDto> currencies = new HashSet<>();

    public void addTransactionCategory(TransactionCategoryDto category) {
        this.transactionCategories.add(category);
    }
    public void addCurrency(CurrencyDto currency) {
        this.currencies.add(currency);
    }
    public void addTransaction(TransactionDto transaction) {
        this.transactions.add(transaction);
    }
    public void addParticipant(ParticipantDto participant) {
        this.participants.add(participant);
    }


    public Set<CurrencyDto> getCurrencies() {
        return currencies;
    }

    public List<TransactionDto> getTransactions() {
        return transactions;
    }

    public List<TransactionCategoryDto> getTransactionCategories() {
        return transactionCategories;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setParticipants(List<ParticipantDto> participants) {
        this.participants = participants;
    }

    public void setTransactions(List<TransactionDto> transactions) {
        this.transactions = transactions;
    }

    public List<ParticipantDto> getParticipants() {
        return participants;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
