package com.teamexpense.rest.dtos.calculation.settle;

import java.math.BigDecimal;

public class DebtDto {

    private long debtorId;
    private long receiverId;
    private BigDecimal amount;

    public DebtDto() {
        this.amount = BigDecimal.ZERO;
    }

    public DebtDto(long debtorId, long receiverId, BigDecimal amount) {
        this.debtorId = debtorId;
        this.receiverId = receiverId;
        this.amount = amount;
    }

    // -------------- Getters and setters -------------


    public long getDebtorId() {
        return debtorId;
    }

    public void setDebtorId(long debtorId) {
        this.debtorId = debtorId;
    }

    public long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(long receiverId) {
        this.receiverId = receiverId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
