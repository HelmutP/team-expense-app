package com.teamexpense.rest.dtos.database;

import java.math.BigDecimal;

public class CurrencyDto extends AbstractDatabaseDto {

    private String name;
    private BigDecimal rate;
    private long tripId;

    public CurrencyDto() {
    }

    public CurrencyDto(String name, BigDecimal rate) {
        this.name = name;
        this.rate = rate;
    }

    public CurrencyDto(String name, BigDecimal rate, long tripId) {
        this(name, rate);
        this.tripId = tripId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public long getTripId() {
        return tripId;
    }

    public void setTripId(long tripId) {
        this.tripId = tripId;
    }
}
