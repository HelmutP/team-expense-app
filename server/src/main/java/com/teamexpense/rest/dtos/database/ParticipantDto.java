package com.teamexpense.rest.dtos.database;

public class ParticipantDto extends AbstractDatabaseDto {

    private String name;
    private long tripId;

    public ParticipantDto() {
    }


    public long getTripId() {
        return tripId;
    }

    public void setTripId(long tripId) {
        this.tripId = tripId;
    }

    public ParticipantDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
