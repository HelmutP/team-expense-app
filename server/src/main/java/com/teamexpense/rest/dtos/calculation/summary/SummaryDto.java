package com.teamexpense.rest.dtos.calculation.summary;

import com.teamexpense.rest.entities.Participant;

import java.util.HashMap;
import java.util.List;

public class SummaryDto {

    private HashMap<Long, SummaryParticipantDto> summaryParticipants;

    public SummaryDto() {
        summaryParticipants = new HashMap<>();
    }

    public SummaryDto(List<Participant> participants) {
        this();
        participants.forEach(p -> {
            summaryParticipants.put(p.getId(), new SummaryParticipantDto(p.getId()));
        });
    }

    public HashMap<Long, SummaryParticipantDto> getSummaryParticipants() {
        return summaryParticipants;
    }

    public void setSummaryParticipants(HashMap<Long, SummaryParticipantDto> summaryParticipants) {
        this.summaryParticipants = summaryParticipants;
    }
}
