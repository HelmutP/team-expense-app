package com.teamexpense.rest.dtos.calculation.settle;

import java.util.ArrayList;
import java.util.List;

public class SettleDto {

    private List<DebtDto> debts;

    public SettleDto() {
        debts = new ArrayList<>();
    }

    public SettleDto(List<DebtDto> debts) {
        this.debts = debts;
    }

    public void addDebt(DebtDto newDebt) {
        this.debts.add(newDebt);
    }


    // -------------- Getters and setters -------------

    public List<DebtDto> getDebts() {
        return debts;
    }

    public void setDebts(List<DebtDto> debts) {
        this.debts = debts;
    }
}
