package com.teamexpense.rest.constants;

public abstract class RestApiConst {

    public static final String REST_API_URL_PREFIX = "/api";

    public static final String REST_API_URL_TRIP = "/trip";
    public static final String REST_API_URL_TRIP_CREATE = REST_API_URL_TRIP + "/for-user";
    public static final String REST_API_URL_TRIPS_OF_USER = REST_API_URL_TRIP + "/user";
    public static final String REST_API_URL_CURRENCIES_OF_TRIP = "/currencies";
    public static final String REST_API_URL_PARTICIPANTS_OF_TRIP = "/participants";

    public static final String REST_API_URL_ADMIN = "/admin";
    public static final String REST_API_URL_ADMIN_ALIVE = REST_API_URL_ADMIN + "/alive";

    public static final String REST_API_URL_USER = "/user";
    public static final String REST_API_URL_USER_LOGIN = REST_API_URL_USER + "/login";
    public static final String REST_API_URL_USER_REGISTER = REST_API_URL_USER + "/register";
    public static final String REST_API_URL_SET_USER_ACTIVE_TRIP = REST_API_URL_USER + "/set-active-trip";

    public static final String REST_API_URL_TRANSACTION = "/transaction";
    public static final String REST_API_URL_TRANSACTION_LATEST = REST_API_URL_TRANSACTION + "/latest";
    public static final String REST_API_URL_TRANSACTION_SEARCH = REST_API_URL_TRANSACTION + "/search";
    public static final String REST_API_URL_TRANSACTION_OF_TRIP = REST_API_URL_TRANSACTION + "/trip";

    private static final String REST_API_URL_CALCULATION = "/calculation";
    public static final String REST_API_URL_CALCULATION_BALANCE = REST_API_URL_CALCULATION + "/balance";
    public static final String REST_API_URL_CALCULATION_SPENT_BY_CATEGORY = REST_API_URL_CALCULATION + "/spent-by-category";;

    public static final String REST_API_RESPONSE_CONTENT_TYPE = "application/json;charset=UTF-8";
}
