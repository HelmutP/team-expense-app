package com.teamexpense.rest.constants;

public abstract class DatabaseConstants {

    public static final String TRIP_TABLE_NAME = "tab_trips";
    public static final String TRANSACTION_TABLE_NAME = "tab_transactions";
    public static final String PARTICIPANT_TABLE_NAME = "tab_participants";
    public static final String TRANSACTION_PARTICIPANT_TABLE_NAME = "tab_transaction_participants";
    public static final String CURRENCY_TABLE_NAME = "tab_currencies";
    public static final String TRANSACTION_CATEGORY_TABLE_NAME = "tab_transaction_categories";
    public static final String USER_TABLE_NAME = "tab_users";
    public static final String USERS_TRIPS_TABLE_NAME = "tab_users_trips";

    public static final String TRIP_ENTITY_NAME = "Trip";
    public static final String TRANSACTION_ENTITY_NAME = "Transaction";
    public static final String PARTICIPANT_ENTITY_NAME = "Participant";
    public static final String TRANSACTION_PARTICIPANT_ENTITY_NAME = "TransactionParticipants";
    public static final String CURRENCY_ENTITY_NAME = "Currency";
    public static final String TRANSACTION_CATEGORY_ENTITY_NAME = "TransactionCategory";
    public static final String USER_ENTITY_NAME = "User";

    public static final String META_COLUMN_ID = "id";
    public static final String META_COLUMN_CREATED = "created_at";
    public static final String META_COLUMN_UPDATED = "updated_at";

    public static final String GENERAL_COLUMN_NAME = "name";
    public static final String GENERAL_COLUMN_DESCRIPTION = "description";
    public static final String GENERAL_COLUMN_AMOUNT = "amount";
    public static final String GENERAL_COLUMN_CURRENCY_TYPE = "currency_type";

    public static final String GENERAL_COLUMN_TRIP_FK = "trip_id";
    public static final String GENERAL_COLUMN_TRANSACTION_FK = "transaction_id";
    public static final String GENERAL_COLUMN_PARTICIPANT_FK = "participant_id";

    public static final String TRANSACTIONS_COLUMN_DATE = "date";
    public static final String TRANSACTIONS_COLUMN_PAYER_FK = "payer_id";
    public static final String TRANSACTIONS_COLUMN_CURRENCY_FK = "currency_fk";
    public static final String TRANSACTIONS_COLUMN_CATEGORY_FK = "category_fk";

    public static final String CURRENCY_COLUMN_CURRENCY_NAME = "name";
    public static final String CURRENCY_COLUMN_RATE = "rate";

    public static final String USER_COLUMN_USERNAME = "username";
    public static final String USER_COLUMN_PASSWORD = "password";
    public static final String USER_COLUMN_SALT = "salt";
    public static final String USER_COLUMN_IBAN = "iban";
    public static final String USER_COLUMN_DISPLAY_NAME = "display_name";
    public static final String USER_COLUMN_ACTIVE_TRIP = "active_trip_fk";

    public static final String USERS_TRIPS_COLUMN_TRIP = "trip_id";
    public static final String USERS_TRIPS_COLUMN_USER = "user_id";
}
