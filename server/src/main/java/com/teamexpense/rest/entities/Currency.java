package com.teamexpense.rest.entities;

import com.teamexpense.rest.constants.DatabaseConstants;
import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity(name = DatabaseConstants.CURRENCY_ENTITY_NAME)
@Table(name = DatabaseConstants.CURRENCY_TABLE_NAME)
public class Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = DatabaseConstants.META_COLUMN_ID, updatable = false, nullable = false)
    private long id;

    @Column(name = DatabaseConstants.META_COLUMN_CREATED, nullable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @Column(name = DatabaseConstants.META_COLUMN_UPDATED, nullable = false)
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @Column(name = DatabaseConstants.CURRENCY_COLUMN_CURRENCY_NAME)
    private String name;

    @Column(name = DatabaseConstants.CURRENCY_COLUMN_RATE, nullable = false)
    private BigDecimal rate;

    @ManyToOne
    @JoinColumn(name = DatabaseConstants.GENERAL_COLUMN_TRIP_FK)
    private Trip trip;

    public Currency() {
    }

    public Currency(String name, BigDecimal rate, Trip trip) {
        this.rate = rate;
        this.name = name;
        this.trip = trip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
