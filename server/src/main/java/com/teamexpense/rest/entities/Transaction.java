package com.teamexpense.rest.entities;

import com.teamexpense.rest.constants.DatabaseConstants;
import com.teamexpense.rest.utils.CollectionsUtils;
import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity(name = DatabaseConstants.TRANSACTION_ENTITY_NAME)
@Table(name = DatabaseConstants.TRANSACTION_TABLE_NAME)
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = DatabaseConstants.META_COLUMN_ID, updatable = false, nullable = false)
    private long id;

    @Column(name = DatabaseConstants.META_COLUMN_CREATED, nullable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @Column(name = DatabaseConstants.META_COLUMN_UPDATED, nullable = false)
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @Column(name = DatabaseConstants.GENERAL_COLUMN_NAME, nullable = false)
    private String name;

    @Column(name = DatabaseConstants.GENERAL_COLUMN_AMOUNT, nullable = false)
    private BigDecimal amount;

    @ManyToOne
    @JoinColumn(name = DatabaseConstants.TRANSACTIONS_COLUMN_CURRENCY_FK)
    private Currency currency;

    @Column(name = DatabaseConstants.TRANSACTIONS_COLUMN_DATE, nullable = false)
    private LocalDate date;

    @Column(name = DatabaseConstants.GENERAL_COLUMN_DESCRIPTION)
    private String description;

    @ManyToOne
    @JoinColumn(name = DatabaseConstants.TRANSACTIONS_COLUMN_PAYER_FK)
    private Participant payer;

    @OneToMany(mappedBy = "transaction", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    List<TransactionParticipant> transactionParticipants = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = DatabaseConstants.TRANSACTIONS_COLUMN_CATEGORY_FK)
    private TransactionCategory category;

    @ManyToOne
    @JoinColumn(name = DatabaseConstants.GENERAL_COLUMN_TRIP_FK)
    private Trip trip;

    public void addParticipantToTransaction(Participant participant, BigDecimal amount) {
        if (this.transactionParticipants.isEmpty() || participant.getId() == 0
                || !this.isParticipantInTransaction(participant.getId())) {
            this.transactionParticipants.add(
                    new TransactionParticipant(this, participant, amount));
        } else {
            throw new IllegalArgumentException(
                    "Participant with id " + participant.getId() + " already exists in " +
                    "transaction with id " + this.getId() + "!");
        }
    }

    public void addParticipantToTransaction(TransactionParticipant transactionParticipant) {
        if (this.transactionParticipants.isEmpty() || transactionParticipant.getParticipant().getId() == 0
                || !this.isParticipantInTransaction(transactionParticipant.getParticipant().getId())) {
            this.transactionParticipants.add(transactionParticipant);
        } else {
            throw new IllegalArgumentException(
                    "Participant with id " + transactionParticipant.getParticipant().getId() +
                    " already exists in transaction with id " + this.getId() + "!");
        }
    }

    protected boolean isParticipantInTransaction(long participantId) {
        return CollectionsUtils.getParticipantFromTransactionParticipants(this.transactionParticipants, participantId) != null;
    }

    public BigDecimal getAmountInEuro() {
        return this.getAmount().divide(this.getCurrency().getRate());
    }


    public TransactionCategory getCategory() {
        return category;
    }

    public void setCategory(TransactionCategory category) {
        this.category = category;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Participant getPayer() {
        return payer;
    }

    public void setPayer(Participant payer) {
        this.payer = payer;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public List<TransactionParticipant> getTransactionParticipants() {
        return transactionParticipants;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
