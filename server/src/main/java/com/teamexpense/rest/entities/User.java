package com.teamexpense.rest.entities;

import com.teamexpense.rest.constants.DatabaseConstants;
import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;
import java.util.List;

@Entity(name = DatabaseConstants.USER_ENTITY_NAME)
@Table(name = DatabaseConstants.USER_TABLE_NAME)
public class User {

    public User() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = DatabaseConstants.META_COLUMN_ID, updatable = false, nullable = false)
    private long id;

    @Column(name = DatabaseConstants.META_COLUMN_CREATED, nullable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @Column(name = DatabaseConstants.META_COLUMN_UPDATED, nullable = false)
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @Column(name = DatabaseConstants.USER_COLUMN_USERNAME, nullable = false)
    private String userName;

    @Column(name = DatabaseConstants.USER_COLUMN_PASSWORD, nullable = false)
    private String password;

    @Column(name = DatabaseConstants.USER_COLUMN_SALT, nullable = false)
    private String salt;

    @Column(name = DatabaseConstants.USER_COLUMN_IBAN)
    private String iban;

    @Column(name = DatabaseConstants.USER_COLUMN_DISPLAY_NAME)
    private String displayName;

    @ManyToOne
    @JoinColumn(name = DatabaseConstants.USER_COLUMN_ACTIVE_TRIP)
    private Trip activeTrip;

    @ManyToMany
    @JoinTable(
            name = DatabaseConstants.USERS_TRIPS_TABLE_NAME,
            joinColumns = @JoinColumn(name = DatabaseConstants.USERS_TRIPS_COLUMN_USER),
            inverseJoinColumns = @JoinColumn(name = DatabaseConstants.USERS_TRIPS_COLUMN_TRIP))
    List<Trip> accessibleTrips;

    public void addAccessToTrip(Trip trip) {
        accessibleTrips.add(trip);
    }


    public List<Trip> getAccessibleTrips() {
        return accessibleTrips;
    }

    public Trip getActiveTrip() {
        return activeTrip;
    }

    public void setActiveTrip(Trip activeTrip) {
        this.activeTrip = activeTrip;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
