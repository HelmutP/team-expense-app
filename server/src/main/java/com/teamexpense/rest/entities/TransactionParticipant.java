package com.teamexpense.rest.entities;

import com.teamexpense.rest.constants.DatabaseConstants;
import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity(name = DatabaseConstants.TRANSACTION_PARTICIPANT_ENTITY_NAME)
@Table(name = DatabaseConstants.TRANSACTION_PARTICIPANT_TABLE_NAME)
public class TransactionParticipant {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = DatabaseConstants.META_COLUMN_ID, updatable = false, nullable = false)
    private long id;

    @Column(name = DatabaseConstants.META_COLUMN_CREATED, nullable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @Column(name = DatabaseConstants.META_COLUMN_UPDATED, nullable = false)
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @Column(name = DatabaseConstants.GENERAL_COLUMN_AMOUNT, nullable = false)
    private BigDecimal amount;

    @ManyToOne
    @JoinColumn(name = DatabaseConstants.GENERAL_COLUMN_TRANSACTION_FK)
    Transaction transaction;

    @ManyToOne
    @JoinColumn(name = DatabaseConstants.GENERAL_COLUMN_PARTICIPANT_FK)
    Participant participant;

    public TransactionParticipant() {
    }

    public TransactionParticipant(Transaction transaction, Participant participant, BigDecimal amount) {
        super();
        this.transaction = transaction;
        this.participant = participant;
        this.amount = amount;
    }

    public BigDecimal getAmountInEuro() {
        return this.getAmount().divide(this.transaction.getCurrency().getRate());
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
