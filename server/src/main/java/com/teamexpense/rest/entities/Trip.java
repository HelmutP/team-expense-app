package com.teamexpense.rest.entities;

import com.teamexpense.rest.constants.DatabaseConstants;
import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;
import java.util.*;

@Entity(name = DatabaseConstants.TRIP_ENTITY_NAME)
@Table(name = DatabaseConstants.TRIP_TABLE_NAME)
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = DatabaseConstants.META_COLUMN_ID, updatable = false, nullable = false)
    private long id;

    @Column(name = DatabaseConstants.META_COLUMN_CREATED, nullable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @Column(name = DatabaseConstants.META_COLUMN_UPDATED, nullable = false)
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @Column(name = DatabaseConstants.GENERAL_COLUMN_NAME, nullable = false)
    private String name;

    @Column(name = DatabaseConstants.GENERAL_COLUMN_DESCRIPTION)
    private String description;

    @OneToMany(mappedBy="trip", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Participant> participants = new ArrayList<>();

    @OneToMany(mappedBy="trip", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Transaction> transactions = new ArrayList<>();

    @OneToMany(mappedBy="trip", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<Currency> currencies = new HashSet<>();

    @OneToMany(mappedBy="trip", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<TransactionCategory> transactionCategories = new ArrayList<>();

    @ManyToMany(mappedBy = "accessibleTrips", cascade = CascadeType.ALL)
    private List<User> accessibleTo;

    public void addAccessibleUser(User user) {
        accessibleTo.add(user);
    }
    public void addCurrency(Currency rate) {
        this.currencies.add(rate);
    }
    public void addTransaction(Transaction transaction) {
        this.transactions.add(transaction);
    }
    public void addParticipant(Participant participant) {
        this.participants.add(participant);
    }
    public void addTransactionCategory(TransactionCategory transactionCategory) {
        this.transactionCategories.add(transactionCategory);
    }


    public List<User> getAccessibleTo() {
        return accessibleTo;
    }

    public List<TransactionCategory> getTransactionCategories() {
        return transactionCategories;
    }

    public Set<Currency> getCurrencies() {
        return currencies;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
