package com.teamexpense.rest.repositories;

import com.teamexpense.rest.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findUserByUserNameEquals(String userName);
}
