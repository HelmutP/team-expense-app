package com.teamexpense.rest.repositories;

import com.teamexpense.rest.entities.Trip;
import com.teamexpense.rest.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TripRepository extends JpaRepository<Trip, Long> {

    List<Trip> findAllByAccessibleToContaining(User user);
}
