package com.teamexpense.rest.repositories;

import com.teamexpense.rest.constants.DatabaseConstants;
import com.teamexpense.rest.entities.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query(value = "SELECT * FROM " + DatabaseConstants.TRANSACTION_TABLE_NAME + " AS transaction " +
            "WHERE (transaction.name LIKE %?1% OR transaction.description LIKE %?1%) AND transaction.trip_id = ?2", nativeQuery = true)
    Set<Transaction> findTransactionsOfTripContainingText(String text, Long tripId);

    List<Transaction> findTop3ByTrip_IdOrderByDateDesc(Long tripId);

    List<Transaction> findAllByTrip_IdOrderByCreatedAtDesc(Long tripId);

    List<Transaction> findAllByTrip_IdOrderByDateDesc(Long tripId);

    List<Transaction> findAllByTrip_IdOrderByUpdatedAtDesc(Long tripId);

    List<Transaction> findAllByTrip_IdOrderByPayerDesc(Long tripId);

    List<Transaction> findAllByTrip_Id(Long tripId);
}
