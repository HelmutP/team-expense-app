package com.teamexpense.rest.mappers;

import com.teamexpense.rest.dtos.database.TransactionDto;
import com.teamexpense.rest.entities.Transaction;
import com.teamexpense.rest.entities.TransactionParticipant;
import com.teamexpense.rest.entities.Trip;
import com.teamexpense.rest.utils.CollectionsUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TransactionMapper {

    private final TransactionParticipantMapper transactionParticipantMapper;
    private final TransactionCategoryMapper transactionCategoryMapper;

    public TransactionMapper(TransactionParticipantMapper transactionParticipantMapper,
                             TransactionCategoryMapper transactionCategoryMapper) {
        this.transactionCategoryMapper = transactionCategoryMapper;
        this.transactionParticipantMapper = transactionParticipantMapper;
    }

    public TransactionDto entityToDto(Transaction transactionEntity) {

        final TransactionDto transactionDto = new TransactionDto();
        this.setMetaDataToDto(transactionDto, transactionEntity);

        transactionDto.setName(transactionEntity.getName());
        transactionDto.setAmount(transactionEntity.getAmount());
        transactionDto.setDate(transactionEntity.getDate());
        transactionDto.setDescription(transactionEntity.getDescription());

        for(TransactionParticipant tp : transactionEntity.getTransactionParticipants()) {
            transactionDto.addTransactionParticipant(transactionParticipantMapper.entityToDto(tp));
        }

        if (transactionEntity.getCurrency() != null) {
            transactionDto.setCurrencyId(transactionEntity.getCurrency().getId());
        }
        if (transactionEntity.getTrip() != null) {
            transactionDto.setTripId(transactionEntity.getTrip().getId());
        }
        if (transactionEntity.getCategory() != null) {
            transactionDto.setCategoryId(transactionEntity.getCategory().getId());
        }
        if (transactionEntity.getPayer() != null) {
            transactionDto.setPayerId(transactionEntity.getPayer().getId());
        }

        return transactionDto;
    }

    public Transaction dtoToEntity(TransactionDto transactionDto, Trip trip) {

        final Transaction transactionEntity = new Transaction();
        this.setMetaDataToEntity(transactionDto, transactionEntity);

        transactionEntity.setName(transactionDto.getName());
        transactionEntity.setAmount(transactionDto.getAmount());
        transactionEntity.setCurrency(CollectionsUtils.getCurrencyById(trip.getCurrencies(), transactionDto.getCurrencyId()));
        transactionEntity.setDate(transactionDto.getDate());
        transactionEntity.setDescription(transactionDto.getDescription());
        transactionEntity.setPayer(CollectionsUtils.getParticipantById(trip.getParticipants(), transactionDto.getPayerId()));
        transactionEntity.setCategory(CollectionsUtils.getTransactionCategoryById(trip.getTransactionCategories(), transactionDto.getCategoryId()));
        transactionDto.getTransactionParticipants().forEach(tp -> {
            transactionEntity.addParticipantToTransaction(
                    transactionParticipantMapper.dtoToEntity(
                            tp, transactionEntity, CollectionsUtils.getParticipantById(trip.getParticipants(), tp.getParticipantId())
                    )
            );
        });
        transactionEntity.setTrip(trip);

        return transactionEntity;
    }

    public List<TransactionDto> entitiesListToDtosList(List<Transaction> entities) {

        final List<TransactionDto> dtos = new ArrayList<>();
        entities.forEach(entity -> {
            dtos.add(entityToDto(entity));
        });
        return dtos;
    }

    protected void setMetaDataToDto(TransactionDto dto, Transaction entity) {
        dto.setId(entity.getId());
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setUpdatedAt(entity.getUpdatedAt());
    }

    protected void setMetaDataToEntity(TransactionDto dto, Transaction entity) {
        entity.setId(dto.getId());
        entity.setCreatedAt(dto.getCreatedAt());
        entity.setUpdatedAt(dto.getUpdatedAt());
    }

}
