package com.teamexpense.rest.mappers;

import com.teamexpense.rest.dtos.database.ParticipantDto;
import com.teamexpense.rest.entities.Participant;
import com.teamexpense.rest.entities.Trip;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ParticipantMapper {

    public ParticipantDto entityToDto(Participant participantEntity) {

        final ParticipantDto participantDto = new ParticipantDto();
        this.setMetaDataToDto(participantDto, participantEntity);

        participantDto.setName(participantEntity.getName());
        if (participantEntity.getTrip() != null) {
            participantDto.setTripId(participantEntity.getTrip().getId());
        }

        return participantDto;
    }

    public Participant dtoToEntity(ParticipantDto participantDto, Trip trip) {

        final Participant participant = new Participant();
        this.setMetaDataToEntity(participantDto, participant);

        participant.setName(participantDto.getName());
        participant.setTrip(trip);

        return participant;
    }

    public List<ParticipantDto> entitiesListToDtosList(List<Participant> entities) {

        final List<ParticipantDto> dtos = new ArrayList<>();
        entities.forEach(entity -> {
            dtos.add(entityToDto(entity));
        });
        return dtos;
    }

    protected void setMetaDataToDto(ParticipantDto dto, Participant entity) {
        dto.setId(entity.getId());
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setUpdatedAt(entity.getUpdatedAt());
    }

    protected void setMetaDataToEntity(ParticipantDto dto, Participant entity) {
        entity.setId(dto.getId());
        entity.setCreatedAt(dto.getCreatedAt());
        entity.setUpdatedAt(dto.getUpdatedAt());
    }

}
