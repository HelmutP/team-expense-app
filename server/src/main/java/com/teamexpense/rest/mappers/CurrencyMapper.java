package com.teamexpense.rest.mappers;

import com.teamexpense.rest.dtos.database.CurrencyDto;
import com.teamexpense.rest.entities.Currency;
import com.teamexpense.rest.entities.Trip;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CurrencyMapper {

    public CurrencyMapper() {
    }

    public CurrencyDto entityToDto(Currency currencyEntity) {

        final CurrencyDto currencyDto = new CurrencyDto();
        this.setMetaDataToDto(currencyDto, currencyEntity);

        currencyDto.setName(currencyEntity.getName());
        currencyDto.setRate(currencyEntity.getRate());

        if (currencyEntity.getTrip() != null) {
            currencyDto.setTripId(currencyEntity.getTrip().getId());
        }

        return currencyDto;
    }

    public Currency dtoToEntity(CurrencyDto currencyDto, Trip trip) {

        final Currency currencyEntity = new Currency();
        this.setMetaDataToEntity(currencyDto, currencyEntity);

        currencyEntity.setName(currencyDto.getName());
        currencyEntity.setRate(currencyDto.getRate());
        currencyEntity.setTrip(trip);

        return currencyEntity;
    }

    public List<CurrencyDto> entitiesListToDtosList(List<Currency> entities) {

        final List<CurrencyDto> dtos = new ArrayList<>();
        entities.forEach(entity -> {
            dtos.add(entityToDto(entity));
        });
        return dtos;
    }

    protected void setMetaDataToDto(CurrencyDto dto, Currency entity) {
        dto.setId(entity.getId());
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setUpdatedAt(entity.getUpdatedAt());
    }

    protected void setMetaDataToEntity(CurrencyDto dto, Currency entity) {
        entity.setId(dto.getId());
        entity.setCreatedAt(dto.getCreatedAt());
        entity.setUpdatedAt(dto.getUpdatedAt());
    }

}
