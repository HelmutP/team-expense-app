package com.teamexpense.rest.mappers;

import com.teamexpense.rest.dtos.database.TransactionCategoryDto;
import com.teamexpense.rest.entities.TransactionCategory;
import com.teamexpense.rest.entities.Trip;
import org.springframework.stereotype.Component;

@Component
public class TransactionCategoryMapper {

    public TransactionCategoryMapper() {
    }

    public TransactionCategoryDto entityToDto(TransactionCategory transCategoryEntity) {

        final TransactionCategoryDto transCategoryDto = new TransactionCategoryDto();
        this.setMetaDataToDto(transCategoryDto, transCategoryEntity);

        transCategoryDto.setName(transCategoryEntity.getName());
        if (transCategoryEntity.getTrip() != null) {
            transCategoryDto.setTripId(transCategoryEntity.getTrip().getId());
        }

        return transCategoryDto;
    }

    public TransactionCategory dtoToEntity(TransactionCategoryDto transCategoryDto, Trip trip) {

        final TransactionCategory transCategoryEntity = new TransactionCategory();
        this.setMetaDataToEntity(transCategoryDto, transCategoryEntity);

        transCategoryEntity.setName(transCategoryDto.getName());
        transCategoryEntity.setTrip(trip);

        return transCategoryEntity;
    }

    protected void setMetaDataToDto(TransactionCategoryDto dto, TransactionCategory entity) {
        dto.setId(entity.getId());
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setUpdatedAt(entity.getUpdatedAt());
    }

    protected void setMetaDataToEntity(TransactionCategoryDto dto, TransactionCategory entity) {
        entity.setId(dto.getId());
        entity.setCreatedAt(dto.getCreatedAt());
        entity.setUpdatedAt(dto.getUpdatedAt());
    }

}
