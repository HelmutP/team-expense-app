package com.teamexpense.rest.mappers;

import com.teamexpense.rest.dtos.database.*;
import com.teamexpense.rest.entities.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TripMapper {

    protected ParticipantMapper participantMapper;
    protected TransactionMapper transactionMapper;
    protected TransactionCategoryMapper transactionCategoryMapper;
    protected CurrencyMapper currencyMapper;

    public TripMapper(ParticipantMapper participantMapper, TransactionMapper transactionMapper,
                      CurrencyMapper currencyMapper, TransactionCategoryMapper transactionCategoryMapper) {
        this.participantMapper = participantMapper;
        this.transactionMapper = transactionMapper;
        this.currencyMapper = currencyMapper;
        this.transactionCategoryMapper = transactionCategoryMapper;
    }

    public TripDto entityToDto(Trip tripEntity) {

        final TripDto tripDto = new TripDto();
        this.setMetaDataToDto(tripDto, tripEntity);

        tripDto.setName(tripEntity.getName());
        tripDto.setDescription(tripEntity.getDescription());
        for(Participant p : tripEntity.getParticipants()) {
            tripDto.addParticipant(participantMapper.entityToDto(p));
        }
        for(TransactionCategory tc : tripEntity.getTransactionCategories()) {
            tripDto.addTransactionCategory(transactionCategoryMapper.entityToDto(tc));
        }
        for(Currency c : tripEntity.getCurrencies()) {
            tripDto.addCurrency(currencyMapper.entityToDto(c));
        }
        for(Transaction t : tripEntity.getTransactions()) {
            tripDto.addTransaction(transactionMapper.entityToDto(t));
        }

        return tripDto;
    }

    public Trip dtoToEntity(TripDto tripDto) {

        final Trip tripEntity = new Trip();
        this.setMetaDataToEntity(tripDto, tripEntity);

        tripEntity.setName(tripDto.getName());
        tripEntity.setDescription(tripDto.getDescription());
        for(ParticipantDto p : tripDto.getParticipants()) {
            tripEntity.addParticipant(participantMapper.dtoToEntity(p, tripEntity));
        }
        for(CurrencyDto c : tripDto.getCurrencies()) {
            tripEntity.addCurrency(currencyMapper.dtoToEntity(c, tripEntity));
        }
        for(TransactionCategoryDto tc : tripDto.getTransactionCategories()) {
            tripEntity.addTransactionCategory(transactionCategoryMapper.dtoToEntity(tc, tripEntity));
        }
        for(TransactionDto t : tripDto.getTransactions()) {
            tripEntity.addTransaction(transactionMapper.dtoToEntity(t, tripEntity));
        }

        return tripEntity;
    }

    public List<TripDto> entitiesListToDtosList(List<Trip> entities) {

        final List<TripDto> dtos = new ArrayList<>();
        entities.forEach(entity -> {
            dtos.add(entityToDto(entity));
        });
        return dtos;
    }

    protected void setMetaDataToDto(TripDto dto, Trip entity) {
        dto.setId(entity.getId());
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setUpdatedAt(entity.getUpdatedAt());
    }

    protected void setMetaDataToEntity(TripDto dto, Trip entity) {
        entity.setId(dto.getId());
        entity.setCreatedAt(dto.getCreatedAt());
        entity.setUpdatedAt(dto.getUpdatedAt());
    }
}
