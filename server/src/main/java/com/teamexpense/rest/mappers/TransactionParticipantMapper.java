package com.teamexpense.rest.mappers;

import com.teamexpense.rest.dtos.database.TransactionParticipantDto;
import com.teamexpense.rest.entities.Participant;
import com.teamexpense.rest.entities.Transaction;
import com.teamexpense.rest.entities.TransactionParticipant;
import org.springframework.stereotype.Component;

@Component
public class TransactionParticipantMapper {

    public TransactionParticipantMapper() {
    }

    public TransactionParticipantDto entityToDto(TransactionParticipant transPartEntity) {

        final TransactionParticipantDto transPartDto = new TransactionParticipantDto();
        this.setMetaDataToDto(transPartDto, transPartEntity);

        transPartDto.setAmount(transPartEntity.getAmount());
        if (transPartEntity.getParticipant() != null) {
            transPartDto.setParticipantId(transPartEntity.getParticipant().getId());
        }
        if (transPartEntity.getTransaction() != null) {
            transPartDto.setTransactionId(transPartEntity.getTransaction().getId());
        }

        return transPartDto;
    }

    public TransactionParticipant dtoToEntity(
            TransactionParticipantDto transPartDto, Transaction transaction, Participant participant) {

        final TransactionParticipant transPartEntity = new TransactionParticipant();
        this.setMetaDataToEntity(transPartDto, transPartEntity);

        transPartEntity.setAmount(transPartDto.getAmount());
        transPartEntity.setTransaction(transaction);
        transPartEntity.setParticipant(participant);

        return transPartEntity;
    }

    protected void setMetaDataToDto(TransactionParticipantDto dto, TransactionParticipant entity) {
        dto.setId(entity.getId());
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setUpdatedAt(entity.getUpdatedAt());
    }

    protected void setMetaDataToEntity(TransactionParticipantDto dto, TransactionParticipant entity) {
        entity.setId(dto.getId());
        entity.setCreatedAt(dto.getCreatedAt());
        entity.setUpdatedAt(dto.getUpdatedAt());
    }
}
