package com.teamexpense.rest.mappers;

import com.teamexpense.rest.dtos.database.*;
import com.teamexpense.rest.entities.*;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public UserMapper() {
    }

    public UserDto entityToDto(User userEntity) {

        final UserDto userDto = new UserDto();
        this.setMetaDataToDto(userDto, userEntity);

        userDto.setDisplayName(userEntity.getDisplayName());
        userDto.setIban(userEntity.getIban());
        userDto.setUserName(userEntity.getUserName());
        userDto.setActiveTripId(userEntity.getActiveTrip() != null ? userEntity.getActiveTrip().getId() : null);

        return userDto;
    }

    public User dtoToEntity(UserDto userDto, Trip activeTrip) {

        final User userEntity = new User();
        this.setMetaDataToEntity(userDto, userEntity);

        userEntity.setDisplayName(userDto.getDisplayName());
        userEntity.setIban(userDto.getIban());
        userEntity.setUserName(userDto.getUserName());
        userEntity.setActiveTrip(activeTrip);

        return userEntity;
    }

    protected void setMetaDataToDto(UserDto dto, User entity) {
        dto.setId(entity.getId());
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setUpdatedAt(entity.getUpdatedAt());
    }

    protected void setMetaDataToEntity(UserDto dto, User entity) {
        entity.setId(dto.getId());
        entity.setCreatedAt(dto.getCreatedAt());
        entity.setUpdatedAt(dto.getUpdatedAt());
    }
}
