package com.teamexpense.rest.utils;

import java.util.Random;

public class StringUtils {

    public static boolean isEmpty(String value) {
        return value == null || value.length() == 0;
    }

    public static String generateRandomStr(int targetStringLength) {

        final int leftAsciiLimit = 48; // numeral '0'
        final int righAsciitLimit = 122; // letter 'z'
        final Random random = new Random();

        return random.ints(leftAsciiLimit, righAsciitLimit + 1)
            .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
            .limit(targetStringLength)
            .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
            .toString();
    }
}
