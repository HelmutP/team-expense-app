package com.teamexpense.rest.utils;

import com.teamexpense.rest.entities.Currency;
import com.teamexpense.rest.entities.Participant;
import com.teamexpense.rest.entities.TransactionCategory;
import com.teamexpense.rest.entities.TransactionParticipant;

import java.util.List;
import java.util.Set;

public class CollectionsUtils {

    public static Participant getParticipantById(List<Participant> participants, long id) {
        return participants.stream().filter(
                p -> p.getId() == id
        ).findFirst().orElse(null);
    }

    public static Participant getParticipantFromTransactionParticipants(
            List<TransactionParticipant> transParts, long participantId) {

        TransactionParticipant transactionWithDesiredParticipant = transParts.stream().filter(
                tp -> tp.getParticipant().getId() == participantId
        ).findFirst().orElse(null);

        return transactionWithDesiredParticipant != null ?
                transactionWithDesiredParticipant.getParticipant()
                : null;
    }

    public static Currency getCurrencyById(Set<Currency> currencies, long currencyId) {
        return currencies.stream().filter(currency -> currency.getId() == currencyId).findFirst().orElse(null);
    }

    public static TransactionCategory getTransactionCategoryById(List<TransactionCategory> transactionCategories, long categoryId) {
        return transactionCategories.stream().filter(category -> category.getId() == categoryId).findFirst().orElse(null);
    }
}
