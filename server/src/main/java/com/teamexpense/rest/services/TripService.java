package com.teamexpense.rest.services;

import com.teamexpense.rest.constants.BusinessConstants;
import com.teamexpense.rest.dtos.database.TripDto;
import com.teamexpense.rest.entities.Currency;
import com.teamexpense.rest.entities.Trip;
import com.teamexpense.rest.entities.Participant;
import com.teamexpense.rest.entities.User;
import com.teamexpense.rest.exceptions.ResourceNotFoundException;
import com.teamexpense.rest.mappers.TripMapper;
import com.teamexpense.rest.repositories.CurrencyRepository;
import com.teamexpense.rest.repositories.TripRepository;
import com.teamexpense.rest.repositories.ParticipantRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Component("TripService")
public class TripService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TripService.class);

    protected final TripRepository tripRepository;
    protected final CurrencyRepository currencyRepository;
    protected final ParticipantRepository participantRepository;
    protected final TripMapper tripMapper;
    protected final UserService userService;

    public TripService(TripRepository tripRepository, CurrencyRepository currencyRepository,
                       ParticipantRepository participantRepository, TripMapper tripMapper, UserService userService) {

        LOGGER.debug("TripService created.");

        this.currencyRepository = currencyRepository;
        this.tripRepository = tripRepository;
        this.tripMapper = tripMapper;
        this.userService = userService;
        this.participantRepository = participantRepository;
    }

    public Trip updateAndSave(Long tripId, TripDto updatedTripDto) throws ResourceNotFoundException {

        final Trip tripToEdit = this.getById(tripId);
        final Trip updatedTrip = tripMapper.dtoToEntity(updatedTripDto);

        tripToEdit.setName(updatedTrip.getName());
        tripToEdit.setDescription(updatedTrip.getDescription());
        tripToEdit.setUpdatedAt(LocalDateTime.now());
        updateParticipantsInTrip(tripToEdit, updatedTrip);
        updateTransactionsInTrip(tripToEdit, updatedTrip);
        updateTransactionCategoriesInTrip(tripToEdit, updatedTrip);

        return tripRepository.saveAndFlush(tripToEdit);
    }

    public Trip getById(Long tripId) throws ResourceNotFoundException {
        return tripRepository.findById(tripId)
                .orElseThrow(() -> new ResourceNotFoundException("Trip with id '" + tripId + "' not found.")
        );
    }

    public Trip create(Trip tripEntityToBeSaved) {

        tripEntityToBeSaved.addCurrency(new Currency(BusinessConstants.EUR_CURRENCY_NAME, new BigDecimal(1), tripEntityToBeSaved));

        final Trip createdBaseTrip = tripRepository.saveAndFlush(tripEntityToBeSaved);

        createdBaseTrip.getParticipants().forEach(participant -> participant.setTrip(createdBaseTrip));
        createdBaseTrip.getCurrencies().forEach(currency -> currency.setTrip(createdBaseTrip));
        createdBaseTrip.getTransactionCategories().forEach(category -> category.setTrip(createdBaseTrip));
        createdBaseTrip.getTransactions().forEach(transaction -> transaction.setTrip(createdBaseTrip));

        return tripRepository.saveAndFlush(createdBaseTrip);
    }

    public boolean delete(Long tripId) throws ResourceNotFoundException {
        try {
            tripRepository.delete(this.getById(tripId));
            return true;
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        }
    }

    public List<Trip> getTripsOfUser(Long userId) {

        try {
            final User user = this.userService.getById(userId);
            return this.tripRepository.findAllByAccessibleToContaining(user);
        } catch (ResourceNotFoundException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    public List<Currency> getTripCurrencies(Long tripId) {
        return currencyRepository.findAllByTrip_Id(tripId);
    }

    public List<Participant> getTripParticipants(Long tripId) {
        return participantRepository.findAllByTrip_Id(tripId);
    }

    // ----------- HELPING METHODS -------------

    private void updateParticipantsInTrip(Trip tripToEdit, Trip editedTrip) {
        tripToEdit.getParticipants().clear();
        editedTrip.getParticipants().forEach(tripToEdit::addParticipant);
    }

    private void updateTransactionsInTrip(Trip tripToEdit, Trip editedTrip) {
        tripToEdit.getTransactions().clear();
        editedTrip.getTransactions().forEach(tripToEdit::addTransaction);
    }

    private void updateTransactionCategoriesInTrip(Trip tripToEdit, Trip editedTrip) {
        tripToEdit.getTransactionCategories().clear();
        editedTrip.getTransactionCategories().forEach(tripToEdit::addTransactionCategory);
    }

}
