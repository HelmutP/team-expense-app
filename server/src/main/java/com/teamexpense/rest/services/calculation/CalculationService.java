package com.teamexpense.rest.services.calculation;

import com.teamexpense.rest.dtos.calculation.settle.SettleDto;
import com.teamexpense.rest.dtos.calculation.summary.SummaryDto;
import com.teamexpense.rest.entities.Participant;
import com.teamexpense.rest.entities.Transaction;
import com.teamexpense.rest.exceptions.ExchangeRateNotFoundException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;

public interface CalculationService {

    SettleDto calculateHowToSettle(SummaryDto summary);

    Map<Long, BigDecimal> calculateSpentByCategory(Long tripId);

    default BigDecimal calculateTotals(List<Transaction> transactions) throws ExchangeRateNotFoundException {

        BigDecimal tmpTotal = BigDecimal.ZERO;
        for (Transaction transaction : transactions) {

            if (transaction.getCurrency().getRate() != null) {
                BigDecimal rate = transaction.getCurrency().getRate();
                BigDecimal amountInEur = transaction.getAmount().divide(rate, RoundingMode.HALF_UP);

                tmpTotal = tmpTotal.add(amountInEur);
            } else {
                throw new ExchangeRateNotFoundException("For currency with id " +
                        transaction.getCurrency().getId() + " exchange rate can not be found.", transaction.getId());
            }
        }
        return tmpTotal;
    }

    default SummaryDto calculateSummary(List<Transaction> transactions, List<Participant> participants) {
        final SummaryDto summary = new SummaryDto(participants);

        transactions.forEach(t -> {
            summary.getSummaryParticipants().get(t.getPayer().getId()).addPaid(t.getAmountInEuro());
            t.getTransactionParticipants().forEach(tp -> {
                summary.getSummaryParticipants().get(tp.getParticipant().getId()).addCharged(tp.getAmountInEuro());
            });
        });
        return summary;
    }
}
