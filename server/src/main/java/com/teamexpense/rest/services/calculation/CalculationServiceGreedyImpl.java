package com.teamexpense.rest.services.calculation;

import com.teamexpense.rest.dtos.calculation.settle.DebtDto;
import com.teamexpense.rest.dtos.calculation.settle.SettleDto;
import com.teamexpense.rest.dtos.calculation.summary.SummaryDto;
import com.teamexpense.rest.dtos.calculation.summary.SummaryParticipantDto;
import com.teamexpense.rest.entities.Transaction;
import com.teamexpense.rest.repositories.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Component("CalculationServiceGreedyImpl")
public class CalculationServiceGreedyImpl implements CalculationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CalculationServiceGreedyImpl.class);

    private final TransactionRepository transactionRepository;

    public CalculationServiceGreedyImpl(TransactionRepository transactionRepository) {
        LOGGER.debug("CalculationServiceGreedyImpl created.");

        this.transactionRepository = transactionRepository;
    }

    @Override
    public SettleDto calculateHowToSettle(SummaryDto summary) {

        // Copy all participants as subgroup (just one subgroup exists)
        final ArrayList<SummaryParticipantDto> subGroupSummaryParticipants = new ArrayList<>();
        if (summary.getSummaryParticipants() != null) {
            summary.getSummaryParticipants().values().forEach(sp -> {
                subGroupSummaryParticipants.add(sp.copy());
            });
        }

        final SettleDto howToSettle = new SettleDto();
        solveSubGroup(subGroupSummaryParticipants, howToSettle);

        return howToSettle;
    }

    @Override
    public Map<Long, BigDecimal> calculateSpentByCategory(Long tripId) {

        final List<Transaction> tripTransactions = this.transactionRepository.findAllByTrip_Id(tripId);
        final Map<Long, BigDecimal> spentByCategories = new HashMap<>();

        tripTransactions.forEach(transaction -> {
            final long currentTransCategoryId = transaction.getCategory().getId();
            if (spentByCategories.containsKey(currentTransCategoryId)) {
                spentByCategories.put(currentTransCategoryId, spentByCategories.get(currentTransCategoryId).add(transaction.getAmount()));
            } else {
                spentByCategories.put(currentTransCategoryId, transaction.getAmount());
            }
        });
        return spentByCategories;
    }

    // ----------------------- Help methods ----------------------

    private void solveSubGroup(ArrayList<SummaryParticipantDto> originGroupParticipants, SettleDto howToSettle) {
        ArrayList<SummaryParticipantDto> currentGroupParticipants = new ArrayList<>(originGroupParticipants);

        while (currentGroupParticipants != null && currentGroupParticipants.size() > 0) {

            currentGroupParticipants = this.orderGroupParticipants(currentGroupParticipants);
            currentGroupParticipants = this.extractHighestPriorityDebtPayment(currentGroupParticipants, howToSettle);
            currentGroupParticipants = this.updateGroupParticipantAmountsAfterExtraction(
                    currentGroupParticipants, howToSettle.getDebts().get(howToSettle.getDebts().size() - 1));
            currentGroupParticipants = this.eliminateAlreadySolvedParticipants(currentGroupParticipants);
        }
    }

    private ArrayList<SummaryParticipantDto> orderGroupParticipants(ArrayList<SummaryParticipantDto> groupParticipants) {
        return (ArrayList<SummaryParticipantDto>) groupParticipants.stream()
                .sorted(Comparator.comparing(SummaryParticipantDto::calculateDifference))
                .collect(Collectors.toList());
    }

    private ArrayList<SummaryParticipantDto> extractHighestPriorityDebtPayment(ArrayList<SummaryParticipantDto> groupParticipants, SettleDto howToSettle) {

        if (groupParticipants != null && groupParticipants.size() > 0) {
            if (groupParticipants.size() == 1) {
                throw new RuntimeException("extractHighestPriorityDebtPayment: list of participants contains just one participant.");
            }
        } else {
            throw new RuntimeException("extractHighestPriorityDebtPayment: Empty list of participants has been provided");
        }

        final SummaryParticipantDto mostPaid = groupParticipants.get(groupParticipants.size() - 1);
        final SummaryParticipantDto mostInDebt = groupParticipants.get(0);
        final BigDecimal debtAmount = mostInDebt.calculateDifference().abs();

        final DebtDto debtRequest = new DebtDto(
                mostInDebt.getParticipantId(), mostPaid.getParticipantId(), debtAmount
        );
        howToSettle.addDebt(debtRequest);

        return groupParticipants;
    }

    private ArrayList<SummaryParticipantDto> updateGroupParticipantAmountsAfterExtraction(
            ArrayList<SummaryParticipantDto> groupParticipants, DebtDto debtRequest) {

        groupParticipants.get(0).addPaid(debtRequest.getAmount());
        groupParticipants.get(groupParticipants.size() - 1).minusPaid(debtRequest.getAmount());

        return groupParticipants;
    }

    private ArrayList<SummaryParticipantDto> eliminateAlreadySolvedParticipants(ArrayList<SummaryParticipantDto> groupParticipants) {

        if (groupParticipants.size() == 2) {
            return (ArrayList<SummaryParticipantDto>) groupParticipants.stream()
                    .filter(p -> p.calculateDifference().longValue() >= 0.01 || p.calculateDifference().longValue() <= -0.01)
                    .collect(Collectors.toList());
        } else {
            return (ArrayList<SummaryParticipantDto>) groupParticipants.stream()
                    .filter(p -> p.calculateDifference().compareTo(BigDecimal.ZERO) != 0)
                    .collect(Collectors.toList());
        }
    }
}
