package com.teamexpense.rest.services;

import com.teamexpense.rest.dtos.database.TransactionDto;
import com.teamexpense.rest.entities.Trip;
import com.teamexpense.rest.entities.Transaction;
import com.teamexpense.rest.exceptions.ResourceNotFoundException;
import com.teamexpense.rest.mappers.TransactionMapper;
import com.teamexpense.rest.repositories.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("TransactionService")
public class TransactionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionService.class);

    private static final String DESC_ORDER_WAY_NAME = "desc";

    protected final TransactionRepository transactionRepository;
    protected final TransactionMapper transactionMapper;

    public TransactionService(TransactionRepository transactionRepository,
                              TransactionMapper transactionMapper) {
        LOGGER.debug("TransactionService created.");

        this.transactionRepository = transactionRepository;
        this.transactionMapper = transactionMapper;
    }

    public Transaction getById(Long transactionId) throws ResourceNotFoundException {
        return transactionRepository.findById(transactionId)
            .orElseThrow(() -> new ResourceNotFoundException("Trip with id '" + transactionId + "' not found.")
        );
    }

    public Transaction create(Transaction transaction) {
        return transactionRepository.saveAndFlush(transaction);
    }

    public Transaction updateAndSave(Long transactionId, TransactionDto transactionDetails, Trip trip) throws ResourceNotFoundException {

        final Transaction transactionToEdit = this.getById(transactionId);
        final Transaction updatedTransaction = transactionMapper.dtoToEntity(transactionDetails, trip);

        transactionToEdit.setName(updatedTransaction.getName());
        transactionToEdit.setAmount(updatedTransaction.getAmount());
        transactionToEdit.setDescription(updatedTransaction.getDescription());
        transactionToEdit.setDate(updatedTransaction.getDate());
        transactionToEdit.setCurrency(updatedTransaction.getCurrency());
        transactionToEdit.setCategory(updatedTransaction.getCategory());
        transactionToEdit.setPayer(updatedTransaction.getPayer());
        transactionToEdit.setTrip(updatedTransaction.getTrip());

        this.updateParticipantsInTransaction(transactionToEdit, updatedTransaction);

        return transactionRepository.saveAndFlush(transactionToEdit);
    }

    public boolean delete(Long transactionId) {
        transactionRepository.deleteById(transactionId);
        return true;
    }

    public List<Transaction> getLatestTransactionsOfTrip(Long tripId) {
        return this.transactionRepository.findTop3ByTrip_IdOrderByDateDesc(tripId);
    }

    public List<Transaction> getOrderedTripTransactions(Long tripId, String column, String orderWay) {

        switch (column) {
            case "date":
                if (orderWay.equals(DESC_ORDER_WAY_NAME)) {
                    return this.getTransactionsOfTripOrderedByDate(tripId);
                }
                break;
            case "created-at":
                if (orderWay.equals(DESC_ORDER_WAY_NAME)) {
                    return this.getTransactionsOfTripOrderedByCreatedAt(tripId);
                }
                break;
            case "payer":
                if (orderWay.equals(DESC_ORDER_WAY_NAME)) {
                    return this.getTransactionsOfTripOrderedByPayer(tripId);
                }
                break;
            case "updated-at":
                if (orderWay.equals(DESC_ORDER_WAY_NAME)) {
                    return this.getTransactionsOfTripOrderedByUpdatedAt(tripId);
                }
                break;
        }
        throw new UnsupportedOperationException();
    }

    public List<Transaction> getTransactionsOfTripOrderedByCreatedAt(Long tripId) {
        return this.transactionRepository.findAllByTrip_IdOrderByCreatedAtDesc(tripId);
    }

    public List<Transaction> getTransactionsOfTripOrderedByDate(Long tripId) {
        return this.transactionRepository.findAllByTrip_IdOrderByDateDesc(tripId);
    }

    public List<Transaction> getTransactionsOfTripOrderedByUpdatedAt(Long tripId) {
        return this.transactionRepository.findAllByTrip_IdOrderByUpdatedAtDesc(tripId);
    }

    public List<Transaction> getTransactionsOfTripOrderedByPayer(Long tripId) {
        return this.transactionRepository.findAllByTrip_IdOrderByPayerDesc(tripId);
    }

    public List<Transaction> getTransactionsOfTripContainingText(String text, Long tripId) {
        return new ArrayList<>(this.transactionRepository.findTransactionsOfTripContainingText(text, tripId));
    }

    private void updateParticipantsInTransaction(Transaction transactionToEdit, Transaction editedTransaction) {
        transactionToEdit.getTransactionParticipants().clear();
        editedTransaction.getTransactionParticipants().forEach(transactionToEdit::addParticipantToTransaction);
    }
}
