package com.teamexpense.rest.services;

import com.teamexpense.rest.communication.requests.LoginUserRequest;
import com.teamexpense.rest.communication.requests.RegisterUserRequest;
import com.teamexpense.rest.entities.Trip;
import com.teamexpense.rest.entities.User;
import com.teamexpense.rest.exceptions.ResourceNotFoundException;
import com.teamexpense.rest.exceptions.UserNotFoundException;
import com.teamexpense.rest.repositories.UserRepository;
import com.teamexpense.rest.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.management.InstanceAlreadyExistsException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Component("UserService")
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private static final int SALT_LENGTH = 10;

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User register(RegisterUserRequest registerUserReq) throws NoSuchAlgorithmException, InstanceAlreadyExistsException {

        if (StringUtils.isEmpty(registerUserReq.getUserName())
                || StringUtils.isEmpty(registerUserReq.getPassword())) {
            throw new RuntimeException("Username or passoword are empty.");
        }

        final User userWithUsername = this.userRepository.findUserByUserNameEquals(registerUserReq.getUserName());
        if (userWithUsername == null) {

            final User registeredUser = new User();
            registeredUser.setUserName(registerUserReq.getUserName());
            registeredUser.setIban(registerUserReq.getIban());
            registeredUser.setDisplayName(registerUserReq.getDisplayName());
            registeredUser.setSalt(StringUtils.generateRandomStr(SALT_LENGTH));
            registeredUser.setPassword(
                    this.getPasswordHash(registerUserReq.getPassword(), registeredUser.getSalt())
            );
            return this.userRepository.saveAndFlush(registeredUser);

        } else {
            throw new InstanceAlreadyExistsException("User with this username already exists");
        }
    }

    public User login(LoginUserRequest loginUserReq) throws UserNotFoundException, NoSuchAlgorithmException {

        if (StringUtils.isEmpty(loginUserReq.getUserName())
                || StringUtils.isEmpty(loginUserReq.getPassword())) {
            throw new RuntimeException("Username or passoword are empty.");
        }

        final User userWithUsername = this.userRepository.findUserByUserNameEquals(loginUserReq.getUserName());
        if (userWithUsername != null) {
            if (this.getPasswordHash(loginUserReq.getPassword(), userWithUsername.getSalt())
                    .equals(userWithUsername.getPassword())) {
                return userWithUsername;
            }
        }

        throw new UserNotFoundException("User with given username and password does not exist.", loginUserReq);
    }

    public User getById(Long userId) throws ResourceNotFoundException {
        return userRepository.findById(userId)
            .orElseThrow(() -> new ResourceNotFoundException("User with id '" + userId + "' not found.")
        );
    }

    public User setActiveTripForUser(User user, Trip trip) {
        user.setActiveTrip(trip);
        this.addAccessToTripIfNotExists(user, trip);
        return userRepository.saveAndFlush(user);
    }

    public User addAccessToTripIfNotExists(User user, Trip trip) {
        if (user.getAccessibleTrips().stream()
                .filter(e -> e.getId() == trip.getId()).findFirst().orElse(null) == null) {
            user.addAccessToTrip(trip);
        }
        return user;
    }

    // ----------------- HELP AUTHENTICATE METHODS --------------------

    private String getPasswordHash(String rawPassword, String salt) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(salt.getBytes(StandardCharsets.UTF_8));
        byte[] bytes = md.digest(rawPassword.getBytes(StandardCharsets.UTF_8));

        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < bytes.length; i++){
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
}
