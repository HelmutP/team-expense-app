package com.teamexpense.rest.exceptions;

public class ExchangeRateNotFoundException extends Exception {

    private long transactionId;

    public ExchangeRateNotFoundException(String message, long transactionId) {
        super(message);
        this.transactionId = transactionId;
    }

    public long getTransactionId() {
        return transactionId;
    }
}
