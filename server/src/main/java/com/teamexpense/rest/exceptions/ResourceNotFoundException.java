package com.teamexpense.rest.exceptions;

public class ResourceNotFoundException extends Exception {

    public ResourceNotFoundException (String message) {
        super(message);
    }
}
