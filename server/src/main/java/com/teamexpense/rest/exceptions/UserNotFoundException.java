package com.teamexpense.rest.exceptions;

import com.teamexpense.rest.communication.requests.LoginUserRequest;

public class UserNotFoundException extends Exception {

    public UserNotFoundException(String message, LoginUserRequest loginUserRequest) {
        super(message);
        this.loginUserRequest = loginUserRequest;
    }

    @Override
    public String getMessage() {
        return super.getMessage() + " Login request: " + loginUserRequest.toString();
    }

    private LoginUserRequest loginUserRequest;

    public LoginUserRequest getLoginUserRequest() {
        return loginUserRequest;
    }
}
