package com.teamexpense.rest.communication.requests;

public class RegisterUserRequest extends LoginUserRequest {

    private String iban;
    private String displayName;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }
}
