package com.teamexpense.rest.communication.responses;

import com.teamexpense.rest.dtos.calculation.settle.SettleDto;
import com.teamexpense.rest.dtos.calculation.summary.SummaryDto;

import java.math.BigDecimal;

public class BalanceResponse {

    private BigDecimal totalSpent;
    private SummaryDto summary;
    private SettleDto settle;


    // ------------ Getters and setters ------------

    public SettleDto getSettle() {
        return settle;
    }

    public void setSettle(SettleDto settle) {
        this.settle = settle;
    }

    public SummaryDto getSummary() {
        return summary;
    }

    public void setSummary(SummaryDto summary) {
        this.summary = summary;
    }

    public BigDecimal getTotalSpent() {
        return totalSpent;
    }

    public void setTotalSpent(BigDecimal totalSpent) {
        this.totalSpent = totalSpent;
    }
}
