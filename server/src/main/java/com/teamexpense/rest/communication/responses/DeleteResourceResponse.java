package com.teamexpense.rest.communication.responses;

public class DeleteResourceResponse {

    private boolean deleted;


    // ------------ Getters and setters ------------

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
