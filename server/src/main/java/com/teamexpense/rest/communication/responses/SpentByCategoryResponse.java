package com.teamexpense.rest.communication.responses;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class SpentByCategoryResponse {

    private Map<Long, BigDecimal> spentByCategory;

    public SpentByCategoryResponse() {
        spentByCategory = new HashMap<>();
    }


    // ------------ Getters and setters ------------

    public Map<Long, BigDecimal> getSpentByCategory() {
        return spentByCategory;
    }

    public void setSpentByCategory(Map<Long, BigDecimal> spentByCategory) {
        this.spentByCategory = spentByCategory;
    }
}
