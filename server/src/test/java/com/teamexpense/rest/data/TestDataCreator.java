package com.teamexpense.rest.data;

import com.teamexpense.rest.communication.requests.LoginUserRequest;
import com.teamexpense.rest.communication.requests.RegisterUserRequest;
import com.teamexpense.rest.constants.BusinessConstants;
import com.teamexpense.rest.dtos.database.*;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

@Component
public class TestDataCreator {

    public TripDto initBaseTrip() {

        TripDto trip = new TripDto();
        trip.setName("TESTtrip!");
        trip.setDescription("TESTDescription");

        trip.addParticipant(new ParticipantDto("Andrej"));
        trip.addParticipant(new ParticipantDto("Andrej2"));
        trip.addParticipant(new ParticipantDto("Andrej3"));

        trip.addTransactionCategory(new TransactionCategoryDto("Category1"));
        trip.addTransactionCategory(new TransactionCategoryDto("Category2"));
        trip.addTransactionCategory(new TransactionCategoryDto("Category3"));

        trip.addCurrency(new CurrencyDto("IRR", new BigDecimal(150000)));
        trip.addCurrency(new CurrencyDto("USD", new BigDecimal(1.15)));

        trip.setCreatedAt(LocalDateTime.of(2009, 3, 5, 0, 0, 0));

        return trip;
    }

    public void addNextTripParticipant(TripDto trip) {
        trip.addParticipant(new ParticipantDto("Dodatocne Doplneny"));
    }

    public void assignSimpleRelationships(TripDto trip) {

        CurrencyDto euroCurrency = (trip.getCurrencies().stream()
                .filter(currencyDto -> currencyDto.getName().equals(BusinessConstants.EUR_CURRENCY_NAME))
                .findFirst()).orElse(null);
        CurrencyDto irrCurrency = (trip.getCurrencies().stream()
                .filter(currencyDto -> currencyDto.getName().equals("IRR"))
                .findFirst()).orElse(null);

        TransactionDto transaction = new TransactionDto(
                "Prva transakcia", new BigDecimal(100.58),
                euroCurrency.getId(),
                trip.getParticipants().get(0).getId()
        );
        transaction.setDescription("Moja prva transakcia");
        transaction.setDate(LocalDate.now());
        transaction.setCategoryId(trip.getTransactionCategories().get(0).getId());
        trip.addTransaction(transaction);
        transaction.addTransactionParticipant(trip.getParticipants().get(1).getId(), new BigDecimal(50.58));
        transaction.addTransactionParticipant(trip.getParticipants().get(2).getId(), new BigDecimal(50));


        TransactionDto transWithCustomCurrency = new TransactionDto(
                "Druha transakcia",
                new BigDecimal(1899),
                irrCurrency.getId(),
                trip.getParticipants().get(0).getId()
        );
        transWithCustomCurrency.setDate(LocalDate.now());
        transWithCustomCurrency.setDescription("IRR as own currency name");
        transWithCustomCurrency.addTransactionParticipant(trip.getParticipants().get(0).getId(), new BigDecimal(1899));
        trip.addTransaction(transWithCustomCurrency);
    }

    public void updateTripData(TripDto trip) {

        CurrencyDto usdCurrency = (trip.getCurrencies().stream()
                .filter(currencyDto -> currencyDto.getName().equals("USD"))
                .findFirst()).orElse(null);

        trip.setName("Zmeneny nazov");
        trip.setDescription("Zmeneny description");
        this.addNextTripParticipant(trip);
        trip.getParticipants().get(0).setName("Zmeneny Participant");

        trip.getTransactions().get(0).setPayerId(trip.getParticipants().get(1).getId());
        trip.getTransactions().get(0).setAmount(new BigDecimal(90.58));
        trip.getTransactions().get(0).setCategoryId(trip.getTransactionCategories().get(1).getId());
        trip.getTransactions().get(0).getTransactionParticipants().clear();
        trip.getTransactions().get(0).addTransactionParticipant(trip.getParticipants().get(0).getId(), new BigDecimal(10.58));
        trip.getTransactions().get(0).addTransactionParticipant(trip.getParticipants().get(2).getId(), new BigDecimal(80));

        trip.getTransactions().get(1).setDate(LocalDate.of(2014, 2, 14));
        trip.getTransactions().get(1).setCurrencyId(usdCurrency.getId());
    }

    public void addComplexTransactionsSet(TripDto trip) {

        final CurrencyDto euroCurrency = (trip.getCurrencies().stream()
                .filter(currencyDto -> currencyDto.getName().equals(BusinessConstants.EUR_CURRENCY_NAME))
                .findFirst()).orElse(null);
        final ArrayList<TransactionDto> complexTransactionsSet = new ArrayList<>();

        complexTransactionsSet.add(new TransactionDto(
                "1", "You are trying deserialize List into Student",
                new BigDecimal(100.58),
                euroCurrency.getId(),
                trip.getParticipants().get(0).getId(),
                LocalDate.of(2018, 6, 7),
                new ArrayList<TransactionParticipantDto>() {{
                    add(new TransactionParticipantDto(
                            new BigDecimal(50.58),
                            trip.getParticipants().get(1).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(50),
                            trip.getParticipants().get(2).getId(),
                            trip.getId())
                    );
                }},
                trip.getTransactionCategories().get(0).getId()
        ));
        complexTransactionsSet.add(new TransactionDto(
                "2", "very detailed description",
                new BigDecimal(214.25),
                euroCurrency.getId(),
                trip.getParticipants().get(2).getId(),
                LocalDate.of(2019, 10, 15),
                new ArrayList<TransactionParticipantDto>() {{
                    add(new TransactionParticipantDto(
                            new BigDecimal(71.424),
                            trip.getParticipants().get(0).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(71.423),
                            trip.getParticipants().get(2).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(71.423),
                            trip.getParticipants().get(3).getId(),
                            trip.getId())
                    );
                }},
                trip.getTransactionCategories().get(0).getId()
        ));

        complexTransactionsSet.add(new TransactionDto(
                "3", "Am I missing something here? I am getting below exception: -",
                new BigDecimal(23.80),
                euroCurrency.getId(),
                trip.getParticipants().get(0).getId(),
                LocalDate.of(2010, 2, 11),
                new ArrayList<TransactionParticipantDto>() {{
                    add(new TransactionParticipantDto(
                            new BigDecimal(9),
                            trip.getParticipants().get(0).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(3.40),
                            trip.getParticipants().get(1).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(8),
                            trip.getParticipants().get(2).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(3.40),
                            trip.getParticipants().get(3).getId(),
                            trip.getId())
                    );
                }},
                trip.getTransactionCategories().get(1).getId()
        ));

        complexTransactionsSet.add(new TransactionDto(
                "4", "You are asking Jackson to parse a StudentList. ",
                new BigDecimal(21),
                euroCurrency.getId(),
                trip.getParticipants().get(3).getId(),
                LocalDate.of(2020, 4, 12),
                new ArrayList<TransactionParticipantDto>() {{
                    add(new TransactionParticipantDto(
                            new BigDecimal(7),
                            trip.getParticipants().get(0).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(7),
                            trip.getParticipants().get(1).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(7),
                            trip.getParticipants().get(2).getId(),
                            trip.getId())
                    );
                }},
                trip.getTransactionCategories().get(1).getId()
        ));

        complexTransactionsSet.add(new TransactionDto(
                "5", "I made a method to do this below called jsonArrayToObjectList",
                new BigDecimal(88),
                euroCurrency.getId(),
                trip.getParticipants().get(0).getId(),
                LocalDate.of(2012, 4, 12),
                new ArrayList<TransactionParticipantDto>() {{
                    add(new TransactionParticipantDto(
                            new BigDecimal(22),
                            trip.getParticipants().get(0).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(22),
                            trip.getParticipants().get(1).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(22),
                            trip.getParticipants().get(2).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(22),
                            trip.getParticipants().get(3).getId(),
                            trip.getId())
                    );
                }},
                trip.getTransactionCategories().get(2).getId()
        ));

        complexTransactionsSet.add(new TransactionDto(
                "6", "Change this to this one",
                new BigDecimal(99),
                euroCurrency.getId(),
                trip.getParticipants().get(0).getId(),
                LocalDate.of(2011, 6, 13),
                new ArrayList<TransactionParticipantDto>() {{
                    add(new TransactionParticipantDto(
                            new BigDecimal(99),
                            trip.getParticipants().get(0).getId(),
                            trip.getId())
                    );
                }},
                trip.getTransactionCategories().get(2).getId()
        ));

        complexTransactionsSet.add(new TransactionDto(
                "7", "I have resolved this one by creating the POJO class (Student.class) of " +
                "the JSON and Main Class is used for read the values from the JSON in the problem.",
                new BigDecimal(152),
                euroCurrency.getId(),
                trip.getParticipants().get(1).getId(),
                LocalDate.of(2015, 5, 14),
                new ArrayList<TransactionParticipantDto>() {{
                    add(new TransactionParticipantDto(
                            new BigDecimal(38),
                            trip.getParticipants().get(0).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(38),
                            trip.getParticipants().get(1).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(38),
                            trip.getParticipants().get(2).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(38),
                            trip.getParticipants().get(3).getId(),
                            trip.getId())
                    );
                }},
                trip.getTransactionCategories().get(2).getId()
        ));

        complexTransactionsSet.add(new TransactionDto(
                "8", "it would be better to include some context",
                new BigDecimal(56),
                euroCurrency.getId(),
                trip.getParticipants().get(3).getId(),
                LocalDate.of(2012, 1, 1),
                new ArrayList<TransactionParticipantDto>() {{
                    add(new TransactionParticipantDto(
                            new BigDecimal(20),
                            trip.getParticipants().get(0).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(10),
                            trip.getParticipants().get(1).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(14),
                            trip.getParticipants().get(2).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(12),
                            trip.getParticipants().get(3).getId(),
                            trip.getId())
                    );
                }},
                trip.getTransactionCategories().get(2).getId()
        ));

        complexTransactionsSet.add(new TransactionDto(
                "9", "@AlexRiabov have added the context to it.",
                new BigDecimal(36),
                euroCurrency.getId(),
                trip.getParticipants().get(2).getId(),
                LocalDate.of(2017, 1, 15),
                new ArrayList<TransactionParticipantDto>() {{
                    add(new TransactionParticipantDto(
                            new BigDecimal(12),
                            trip.getParticipants().get(0).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(12),
                            trip.getParticipants().get(2).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(12),
                            trip.getParticipants().get(3).getId(),
                            trip.getId())
                    );
                }},
                trip.getTransactionCategories().get(2).getId()
        ));

        complexTransactionsSet.add(new TransactionDto(
                "10", "This is for the scam on the Westminister bridge",
                new BigDecimal(223),
                euroCurrency.getId(),
                trip.getParticipants().get(0).getId(),
                LocalDate.of(2013, 11, 8),
                new ArrayList<TransactionParticipantDto>() {{
                    add(new TransactionParticipantDto(
                            new BigDecimal(111.50),
                            trip.getParticipants().get(1).getId(),
                            trip.getId())
                    );
                    add(new TransactionParticipantDto(
                            new BigDecimal(111.50),
                            trip.getParticipants().get(2).getId(),
                            trip.getId())
                    );
                }},
                trip.getTransactionCategories().get(2).getId()
        ));

        trip.setTransactions(complexTransactionsSet);
    }

    public RegisterUserRequest createUserRegisterReq() {

        final RegisterUserRequest userRegister = new RegisterUserRequest();
        userRegister.setIban("SK3696161587349298855829");
        userRegister.setDisplayName("Prvý Používateľ");
        userRegister.setUserName(LocalDateTime.now().toString());
        userRegister.setPassword("b109f3bbbc244eb82441917ed06d618b9008dd09b3befd1b5e07394c706a8bb980b1d7785" +
                "e5976ec049b46df5f1326af5a2ea6d103fd07c95385ffab0cacbc86");
        return userRegister;
    }

    public LoginUserRequest createLoginUserReq(boolean setCorrectPassword, String userName) {
        final LoginUserRequest userLogin = new LoginUserRequest();

        userLogin.setUserName(userName);
        if (setCorrectPassword) {
            userLogin.setPassword("b109f3bbbc244eb82441917ed06d618b9008dd09b3befd1b5e07394c706a8bb980b1d7785" +
                    "e5976ec049b46df5f1326af5a2ea6d103fd07c95385ffab0cacbc86");
        } else {
            userLogin.setPassword("AAAAA3bbbc244eb82441917ed06d618b9008dd09b3befd1b5e07394c706a8bb980b1d7785" +
                    "e5976ec049b46df5f1326af5a2ea6d103fd07c95385ffab0cacbc86");
        }
        return userLogin;
    }
}
