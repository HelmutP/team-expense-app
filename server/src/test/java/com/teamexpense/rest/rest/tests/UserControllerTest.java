package com.teamexpense.rest.rest.tests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.teamexpense.rest.communication.requests.LoginUserRequest;
import com.teamexpense.rest.communication.requests.RegisterUserRequest;
import com.teamexpense.rest.constants.RestApiConst;
import com.teamexpense.rest.dtos.database.UserDto;
import com.teamexpense.rest.rest.tests.abstracts.TripDependentControllerTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.*;

@SpringBootTest
public class UserControllerTest extends TripDependentControllerTest {

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test
    @Order(1)
    public void testConnection() throws Exception {
        apiUserController.perform(MockMvcRequestBuilders.options(RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_USER_LOGIN)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(res -> {
            assertEquals(200, res.getResponse().getStatus());
        });
    }

    @Test
    @Order(2)
    public void register() throws Exception {

        final RegisterUserRequest request = this.testDataCreator.createUserRegisterReq();
        final String inputJson = jsonProcessingUtils.mapToJson(request);

        apiUserController.perform(MockMvcRequestBuilders.post(RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_USER_REGISTER)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)
        ).andDo(res -> {
            assertEquals(200, res.getResponse().getStatus());

            final UserDto registeredUser = jsonProcessingUtils.mapFromJson(
                    res.getResponse().getContentAsString(), new TypeReference<UserDto>(){});

            assertNotNull(registeredUser);
            assertNotNull(registeredUser.getId());
            assertNotNull(registeredUser.getCreatedAt());
            assertNotNull(registeredUser.getUpdatedAt());
            assertEquals(registeredUser.getDisplayName(), request.getDisplayName());
            assertEquals(registeredUser.getIban(), request.getIban());
            assertEquals(registeredUser.getUserName(), request.getUserName());

            this.createdUser = registeredUser;
        });
    }

    @Test
    @Order(3)
    public void login() throws Exception {

        final LoginUserRequest request = this.testDataCreator.createLoginUserReq(
                true, this.createdUser.getUserName());
        final String inputJson = jsonProcessingUtils.mapToJson(request);

        apiUserController.perform(MockMvcRequestBuilders.post(RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_USER_LOGIN)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)
        ).andDo(res -> {
            assertEquals(200, res.getResponse().getStatus());

            final UserDto loggedInUser = jsonProcessingUtils.mapFromJson(
                    res.getResponse().getContentAsString(), new TypeReference<UserDto>(){});

            assertNotNull(loggedInUser);
            assertNotNull(loggedInUser.getId());
            assertNotNull(loggedInUser.getCreatedAt());
            assertNotNull(loggedInUser.getUpdatedAt());
            assertEquals(loggedInUser.getDisplayName(), "Prvý Používateľ");
            assertEquals(loggedInUser.getIban(), "SK3696161587349298855829");
            assertEquals(loggedInUser.getUserName(), request.getUserName());
        });
    }

    @Test
    @Order(4)
    public void tryTologinWithWrongPassword() throws Exception {

        final LoginUserRequest request = this.testDataCreator.createLoginUserReq(
                false, this.createdUser.getUserName());
        final String inputJson = jsonProcessingUtils.mapToJson(request);

        apiUserController.perform(MockMvcRequestBuilders.post(RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_USER_LOGIN)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)
        ).andDo(res -> {
            assertEquals(res.getResponse().getStatus(), 400);
        });
    }

    @Test
    @Order(5)
    public void setActiveTrip() throws Exception {

        apiUserController.perform(MockMvcRequestBuilders.get(
                RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_SET_USER_ACTIVE_TRIP +
                        "/" + this.createdUser.getId() + "/" + this.createdSimpleTripDto.getId())
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(res -> {
            assertEquals(200, res.getResponse().getStatus());

            final UserDto userWithActiveTrip = jsonProcessingUtils.mapFromJson(
                    res.getResponse().getContentAsString(), new TypeReference<UserDto>(){});

            assertNotNull(userWithActiveTrip);
            assertNotNull(userWithActiveTrip.getId());
            assertNotNull(userWithActiveTrip.getCreatedAt());
            assertNotNull(userWithActiveTrip.getUpdatedAt());
            assertEquals(userWithActiveTrip.getDisplayName(), this.createdUser.getDisplayName());
            assertEquals(userWithActiveTrip.getIban(), this.createdUser.getIban());
            assertEquals(userWithActiveTrip.getUserName(), this.createdUser.getUserName());
            assertNotNull(userWithActiveTrip.getActiveTripId());
            assertEquals(this.createdSimpleTripDto.getId(), userWithActiveTrip.getActiveTripId().longValue());
        });
    }
}
