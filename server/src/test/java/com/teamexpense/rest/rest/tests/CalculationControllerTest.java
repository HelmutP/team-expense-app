package com.teamexpense.rest.rest.tests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.teamexpense.rest.communication.responses.BalanceResponse;
import com.teamexpense.rest.communication.responses.SpentByCategoryResponse;
import com.teamexpense.rest.constants.RestApiConst;
import com.teamexpense.rest.controllers.CalculationController;
import com.teamexpense.rest.dtos.calculation.settle.DebtDto;
import com.teamexpense.rest.dtos.calculation.summary.SummaryParticipantDto;
import com.teamexpense.rest.rest.tests.abstracts.TripDependentControllerTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class CalculationControllerTest extends TripDependentControllerTest {

    @Autowired
    private CalculationController calculationController;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        this.api = MockMvcBuilders.standaloneSetup(calculationController).build();
    }

    @Test
    @Order(1)
    public void testConnection() throws Exception {
        api.perform(MockMvcRequestBuilders.options(RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_CALCULATION_BALANCE + "/" + this.createdSimpleTripDto.getId())
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(res -> {
            assertEquals(200, res.getResponse().getStatus());
        });
    }

    @Test
    @Order(2)
    public void testSimpleCase() throws Exception {
        api.perform(MockMvcRequestBuilders.get(RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_CALCULATION_BALANCE + "/" + this.createdSimpleTripDto.getId())
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(res -> {
            assertEquals(200, res.getResponse().getStatus());

            final BalanceResponse balance = jsonProcessingUtils.mapFromJson(
                    res.getResponse().getContentAsString(), new TypeReference<BalanceResponse>(){});

            final long firstParticipantId = this.createdSimpleTripDto.getParticipants().get(0).getId();
            final long secondParticipantId = this.createdSimpleTripDto.getParticipants().get(1).getId();
            final long thirdParticipantId = this.createdSimpleTripDto.getParticipants().get(2).getId();
            final long fourthParticipantId = this.createdSimpleTripDto.getParticipants().get(3).getId();
            final HashMap<Long, SummaryParticipantDto> summaryParticipants = balance.getSummary().getSummaryParticipants();
            final ArrayList<DebtDto> debts = (ArrayList<DebtDto>) balance.getSettle().getDebts().stream()
                    .sorted(Comparator.comparing(DebtDto::getAmount))
                    .collect(Collectors.toList());

            assertEquals(new BigDecimal(1741.88).longValue(), balance.getTotalSpent().longValue());

            assertEquals(summaryParticipants.get(firstParticipantId).getPaid().longValue(), new BigDecimal(1651.30).longValue());
            assertEquals(summaryParticipants.get(firstParticipantId).getCharged().longValue(), new BigDecimal(1661.88).longValue());
            assertEquals(summaryParticipants.get(firstParticipantId).calculateDifference().longValue(), new BigDecimal(-10.58).longValue());

            assertEquals(summaryParticipants.get(secondParticipantId).getPaid().longValue(), new BigDecimal(90.58).longValue());
            assertEquals(summaryParticipants.get(secondParticipantId).getCharged().longValue(), BigDecimal.ZERO.longValue());
            assertEquals(summaryParticipants.get(secondParticipantId).calculateDifference().longValue(), new BigDecimal(90.58).longValue());

            assertEquals(summaryParticipants.get(thirdParticipantId).getPaid().longValue(), BigDecimal.ZERO.longValue());
            assertEquals(summaryParticipants.get(thirdParticipantId).getCharged().longValue(), new BigDecimal(80).longValue());
            assertEquals(summaryParticipants.get(thirdParticipantId).calculateDifference().longValue(), new BigDecimal(-80).longValue());

            assertEquals(summaryParticipants.get(fourthParticipantId).getPaid().longValue(), BigDecimal.ZERO.longValue());
            assertEquals(summaryParticipants.get(fourthParticipantId).getCharged().longValue(), BigDecimal.ZERO.longValue());
            assertEquals(summaryParticipants.get(fourthParticipantId).calculateDifference().longValue(), BigDecimal.ZERO.longValue());

            assertEquals(debts.get(0).getAmount().longValue(), new BigDecimal(10.58).longValue());
            assertEquals(debts.get(0).getDebtorId(), firstParticipantId);
            assertEquals(debts.get(0).getReceiverId(), secondParticipantId);

            assertEquals(debts.get(1).getAmount().longValue(), new BigDecimal(80).longValue());
            assertEquals(debts.get(1).getDebtorId(), thirdParticipantId);
            assertEquals(debts.get(1).getReceiverId(), secondParticipantId);
        });
    }

    @Test
    @Order(3)
    public void testComplexTransactionsSetCase() throws Exception {
        api.perform(MockMvcRequestBuilders.get(RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_CALCULATION_BALANCE + "/" + this.createdComplexTripDto.getId())
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(res -> {
            assertEquals(200, res.getResponse().getStatus());

            final BalanceResponse balance = jsonProcessingUtils.mapFromJson(
                    res.getResponse().getContentAsString(), new TypeReference<BalanceResponse>(){});
            final long firstParticipantId = this.createdComplexTripDto.getParticipants().get(0).getId();
            final long secondParticipantId = this.createdComplexTripDto.getParticipants().get(1).getId();
            final long thirdParticipantId = this.createdComplexTripDto.getParticipants().get(2).getId();
            final long fourthParticipantId = this.createdComplexTripDto.getParticipants().get(3).getId();
            final HashMap<Long, SummaryParticipantDto> summaryParticipants = balance.getSummary().getSummaryParticipants();
            final ArrayList<DebtDto> debts = (ArrayList<DebtDto>) balance.getSettle().getDebts().stream()
                    .sorted(Comparator.comparing(DebtDto::getAmount))
                    .collect(Collectors.toList());

            assertEquals(new BigDecimal(1013.63).longValue(), balance.getTotalSpent().longValue());

            assertEquals(summaryParticipants.get(firstParticipantId).getPaid().longValue(), new BigDecimal(534.38).longValue());
            assertEquals(summaryParticipants.get(firstParticipantId).getCharged().longValue(), new BigDecimal(278.42).longValue());
            assertEquals(summaryParticipants.get(firstParticipantId).calculateDifference().longValue(), new BigDecimal(255.96).longValue());

            assertEquals(summaryParticipants.get(secondParticipantId).getPaid().longValue(), new BigDecimal(152).longValue());
            assertEquals(summaryParticipants.get(secondParticipantId).getCharged().longValue(), new BigDecimal(242.48).longValue());
            assertEquals(summaryParticipants.get(secondParticipantId).calculateDifference().longValue(), new BigDecimal(-90.48).longValue());

            assertEquals(summaryParticipants.get(thirdParticipantId).getPaid().longValue(), new BigDecimal(250.25).longValue());
            assertEquals(summaryParticipants.get(thirdParticipantId).getCharged().longValue(), new BigDecimal(333.92).longValue());
            assertEquals(summaryParticipants.get(thirdParticipantId).calculateDifference().longValue(), new BigDecimal(-83.67).longValue());

            assertEquals(summaryParticipants.get(fourthParticipantId).getPaid().longValue(), new BigDecimal(77).longValue());
            assertEquals(summaryParticipants.get(fourthParticipantId).getCharged().longValue(), new BigDecimal(158.82).longValue());
            assertEquals(summaryParticipants.get(fourthParticipantId).calculateDifference().longValue(), new BigDecimal(-81.82).longValue());

            assertEquals(debts.get(0).getAmount().longValue(), new BigDecimal(81.82).longValue());
            assertEquals(debts.get(0).getDebtorId(), fourthParticipantId);
            assertEquals(debts.get(0).getReceiverId(), firstParticipantId);

            assertEquals(debts.get(1).getAmount().longValue(), new BigDecimal(83.67).longValue());
            assertEquals(debts.get(1).getDebtorId(), thirdParticipantId);
            assertEquals(debts.get(1).getReceiverId(), firstParticipantId);

            assertEquals(debts.get(2).getAmount().longValue(), new BigDecimal(90.48).longValue());
            assertEquals(debts.get(2).getDebtorId(), secondParticipantId);
            assertEquals(debts.get(2).getReceiverId(), firstParticipantId);
        });
    }

    @Test
    @Order(4)
    public void testSpentByCategory() throws Exception {
        api.perform(MockMvcRequestBuilders.get(RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_CALCULATION_SPENT_BY_CATEGORY+ "/" + this.createdComplexTripDto.getId())
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(res -> {
            assertEquals(200, res.getResponse().getStatus());

            final SpentByCategoryResponse response = jsonProcessingUtils.mapFromJson(
                    res.getResponse().getContentAsString(), new TypeReference<SpentByCategoryResponse>(){});

            assertEquals(new BigDecimal(314.83).longValue(),
                    response.getSpentByCategory().get(this.createdComplexTripDto.getTransactionCategories().get(0).getId()).longValue());
            assertEquals(new BigDecimal(44.8).longValue(),
                    response.getSpentByCategory().get(this.createdComplexTripDto.getTransactionCategories().get(1).getId()).longValue());
            assertEquals(new BigDecimal(654).longValue(),
                    response.getSpentByCategory().get(this.createdComplexTripDto.getTransactionCategories().get(2).getId()).longValue());
        });
    }
}
