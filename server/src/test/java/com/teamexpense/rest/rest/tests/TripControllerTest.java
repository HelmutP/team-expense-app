package com.teamexpense.rest.rest.tests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.teamexpense.rest.communication.responses.DeleteResourceResponse;
import com.teamexpense.rest.constants.BusinessConstants;
import com.teamexpense.rest.constants.RestApiConst;
import com.teamexpense.rest.controllers.TripController;
import com.teamexpense.rest.dtos.database.CurrencyDto;
import com.teamexpense.rest.dtos.database.TripDto;
import com.teamexpense.rest.rest.tests.abstracts.AbstractControllerTest;
import com.teamexpense.rest.services.TripService;
import com.teamexpense.rest.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

@SpringBootTest
public class TripControllerTest extends AbstractControllerTest {

    @Autowired
    private TripController tripControllerMock;

    private TripDto createdTripDto;

    @Mock
    TripService tripService;

    @Mock
    UserService userService;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        this.api = MockMvcBuilders.standaloneSetup(tripControllerMock).build();
    }

    @Test
    @Order(1)
    public void testConnection() throws Exception {
        api.perform(MockMvcRequestBuilders.options(
                RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRIP_CREATE + "/" + this.createdUser.getId())
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(res -> {
            assertEquals(200, res.getResponse().getStatus());
        });
    }

    @Test
    @Order(2)
    public void createTrip() throws Exception {

        String inputJson = jsonProcessingUtils.mapToJson(this.testDataCreator.initBaseTrip());

         api.perform(MockMvcRequestBuilders.post(
                 RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRIP_CREATE + "/" + this.createdUser.getId())
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)
                ).andDo(res1 -> {

             assertEquals(200, res1.getResponse().getStatus());
             assertNotNull(res1.getResponse().getContentAsString());

             TripDto createdTrip = jsonProcessingUtils.mapFromJson(
                     res1.getResponse().getContentAsString(), new TypeReference<TripDto>(){});
             this.testDataCreator.assignSimpleRelationships(createdTrip);

             String inputJsonWithRelations = jsonProcessingUtils.mapToJson(createdTrip);

             api.perform(MockMvcRequestBuilders.put(
                     RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRIP + "/" + createdTrip.getId())
                     .accept(MediaType.APPLICATION_JSON_VALUE)
                     .contentType(MediaType.APPLICATION_JSON_VALUE)
                     .content(inputJsonWithRelations)
             ).andDo(res2 -> {

                 assertEquals(200, res2.getResponse().getStatus());
                 assertNotNull(res2.getResponse().getContentAsString());

                 TripDto createdTripWitRelations = jsonProcessingUtils.mapFromJson(
                         res2.getResponse().getContentAsString(), new TypeReference<TripDto>(){});
                 CurrencyDto euroCurrency = (createdTripWitRelations.getCurrencies().stream()
                         .filter(currencyDto -> currencyDto.getName().equals(BusinessConstants.EUR_CURRENCY_NAME))
                         .findFirst()).orElse(null);
                 CurrencyDto irrCurrency = (createdTripWitRelations.getCurrencies().stream()
                         .filter(currencyDto -> currencyDto.getName().equals("IRR"))
                         .findFirst()).orElse(null);

                 assertNotNull(createdTripWitRelations.getId());
                 assertEquals(createdTripWitRelations.getName(), "TESTtrip!");
                 assertEquals(createdTripWitRelations.getDescription(), "TESTDescription");
                 assertNotNull(createdTripWitRelations.getCreatedAt());
                 assertNotNull(createdTripWitRelations.getUpdatedAt());

                 assertEquals(createdTripWitRelations.getParticipants().size(), 3);
                 assertEquals(createdTripWitRelations.getParticipants().get(0).getName(), "Andrej");
                 assertEquals(createdTripWitRelations.getParticipants().get(1).getName(), "Andrej2");
                 assertEquals(createdTripWitRelations.getParticipants().get(2).getName(), "Andrej3");

                 assertEquals(createdTripWitRelations.getTransactionCategories().size(), 3);
                 assertEquals(createdTripWitRelations.getTransactionCategories().get(0).getName(), "Category1");
                 assertEquals(createdTripWitRelations.getTransactionCategories().get(1).getName(), "Category2");
                 assertEquals(createdTripWitRelations.getTransactionCategories().get(2).getName(), "Category3");

                 assertEquals(createdTripWitRelations.getTransactions().size(), 2);

                 assertEquals(createdTripWitRelations.getTransactions().get(0).getName(), "Prva transakcia");
                 assertEquals(createdTripWitRelations.getTransactions().get(0).getDescription(), "Moja prva transakcia");
                 assertEquals(createdTripWitRelations.getTransactions().get(0).getCurrencyId(), euroCurrency.getId());
                 assertEquals(createdTripWitRelations.getTransactions().get(0).getCategoryId(), createdTripWitRelations.getTransactionCategories().get(0).getId());
                 assertEquals(createdTripWitRelations.getTransactions().get(0).getPayerId(), createdTripWitRelations.getParticipants().get(0).getId());
                 assertEquals(createdTripWitRelations.getTransactions().get(0).getAmount().longValue(), new BigDecimal(100.58).longValue());
                 assertEquals(createdTripWitRelations.getTransactions().get(0).getTransactionParticipants().size(), 2);
                 assertEquals(createdTripWitRelations.getTransactions().get(0).getTransactionParticipants().get(0).getParticipantId(), createdTripWitRelations.getParticipants().get(1).getId());
                 assertEquals(createdTripWitRelations.getTransactions().get(0).getTransactionParticipants().get(0).getAmount().longValue(), new BigDecimal(50.58).longValue());
                 assertEquals(createdTripWitRelations.getTransactions().get(0).getTransactionParticipants().get(1).getParticipantId(), createdTripWitRelations.getParticipants().get(2).getId());
                 assertEquals(createdTripWitRelations.getTransactions().get(0).getTransactionParticipants().get(1).getAmount().longValue(), new BigDecimal(50).longValue());

                 assertEquals(createdTripWitRelations.getTransactions().get(1).getName(), "Druha transakcia");
                 assertEquals(createdTripWitRelations.getTransactions().get(1).getDescription(), "IRR as own currency name");
                 assertEquals(createdTripWitRelations.getTransactions().get(1).getCurrencyId(), irrCurrency.getId());
                 assertEquals(createdTripWitRelations.getTransactions().get(1).getCategoryId(), 0);
                 assertEquals(createdTripWitRelations.getTransactions().get(1).getPayerId(), createdTripWitRelations.getParticipants().get(0).getId());
                 assertEquals(createdTripWitRelations.getTransactions().get(1).getAmount().longValue(), new BigDecimal(1899).longValue());
                 assertEquals(createdTripWitRelations.getTransactions().get(1).getTransactionParticipants().size(), 1);
                 assertEquals(createdTripWitRelations.getTransactions().get(1).getTransactionParticipants().get(0).getParticipantId(), createdTripWitRelations.getParticipants().get(0).getId());
                 assertEquals(createdTripWitRelations.getTransactions().get(1).getTransactionParticipants().get(0).getAmount().longValue(), new BigDecimal(1899).longValue());

                 this.createdTripDto = createdTripWitRelations;
             });
         });
    }

    @Test
    @Order(3)
    public void getTrip() throws Exception {

        if (this.createdTripDto == null) {
            this.createTrip();
        }

        api.perform(MockMvcRequestBuilders.get(
                RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRIP + "/" + this.createdTripDto.getId())
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(res -> {
            TripDto returnedTrip = jsonProcessingUtils.mapFromJson(
                    res.getResponse().getContentAsString(), new TypeReference<TripDto>(){});
            CurrencyDto euroCurrency = (returnedTrip.getCurrencies().stream()
                    .filter(currencyDto -> currencyDto.getName().equals(BusinessConstants.EUR_CURRENCY_NAME))
                    .findFirst()).orElse(null);
            CurrencyDto irrCurrency = (returnedTrip.getCurrencies().stream()
                    .filter(currencyDto -> currencyDto.getName().equals("IRR"))
                    .findFirst()).orElse(null);

            assertEquals(200, res.getResponse().getStatus());
            assertNotNull(returnedTrip);
            assertEquals(returnedTrip.getId(), this.createdTripDto.getId());

            assertEquals(returnedTrip.getName(), "TESTtrip!");
            assertEquals(returnedTrip.getDescription(), "TESTDescription");
            assertNotNull(returnedTrip.getCreatedAt());
            assertNotNull(returnedTrip.getUpdatedAt());

            assertEquals(returnedTrip.getParticipants().size(), 3);
            assertEquals(returnedTrip.getParticipants().get(0).getName(), "Andrej");
            assertEquals(returnedTrip.getParticipants().get(1).getName(), "Andrej2");
            assertEquals(returnedTrip.getParticipants().get(2).getName(), "Andrej3");

            assertEquals(returnedTrip.getTransactionCategories().size(), 3);
            assertEquals(returnedTrip.getTransactionCategories().get(0).getName(), "Category1");
            assertEquals(returnedTrip.getTransactionCategories().get(1).getName(), "Category2");
            assertEquals(returnedTrip.getTransactionCategories().get(2).getName(), "Category3");

            assertEquals(returnedTrip.getTransactions().size(), 2);

            assertEquals(returnedTrip.getTransactions().get(0).getName(), "Prva transakcia");
            assertEquals(returnedTrip.getTransactions().get(0).getDescription(), "Moja prva transakcia");
            assertEquals(returnedTrip.getTransactions().get(0).getCurrencyId(), euroCurrency.getId());
            assertEquals(returnedTrip.getTransactions().get(0).getCategoryId(), returnedTrip.getTransactionCategories().get(0).getId());
            assertEquals(returnedTrip.getTransactions().get(0).getPayerId(), returnedTrip.getParticipants().get(0).getId());
            assertEquals(returnedTrip.getTransactions().get(0).getAmount().longValue(), new BigDecimal(100.58).longValue());
            assertEquals(returnedTrip.getTransactions().get(0).getTransactionParticipants().size(), 2);
            assertEquals(returnedTrip.getTransactions().get(0).getTransactionParticipants().get(0).getParticipantId(), returnedTrip.getParticipants().get(1).getId());
            assertEquals(returnedTrip.getTransactions().get(0).getTransactionParticipants().get(0).getAmount().longValue(), new BigDecimal(50.58).longValue());
            assertEquals(returnedTrip.getTransactions().get(0).getTransactionParticipants().get(1).getParticipantId(), returnedTrip.getParticipants().get(2).getId());
            assertEquals(returnedTrip.getTransactions().get(0).getTransactionParticipants().get(1).getAmount().longValue(), new BigDecimal(50).longValue());

            assertEquals(returnedTrip.getTransactions().get(1).getName(), "Druha transakcia");
            assertEquals(returnedTrip.getTransactions().get(1).getDescription(), "IRR as own currency name");
            assertEquals(returnedTrip.getTransactions().get(1).getCurrencyId(), irrCurrency.getId());
            assertEquals(returnedTrip.getTransactions().get(1).getCategoryId(), 0);
            assertEquals(returnedTrip.getTransactions().get(1).getPayerId(), returnedTrip.getParticipants().get(0).getId());
            assertEquals(returnedTrip.getTransactions().get(1).getAmount().longValue(), new BigDecimal(1899).longValue());
            assertEquals(returnedTrip.getTransactions().get(1).getTransactionParticipants().size(), 1);
            assertEquals(returnedTrip.getTransactions().get(1).getTransactionParticipants().get(0).getParticipantId(), returnedTrip.getParticipants().get(0).getId());
            assertEquals(returnedTrip.getTransactions().get(1).getTransactionParticipants().get(0).getAmount().longValue(), new BigDecimal(1899).longValue());

        });
    }

    @Test
    @Order(4)
    public void updateTrip() throws Exception {

        if (this.createdTripDto == null) {
            this.createTrip();
        }

        this.testDataCreator.updateTripData(this.createdTripDto);
        String inputJson = jsonProcessingUtils.mapToJson(this.createdTripDto);

        api.perform(MockMvcRequestBuilders.put(
                RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRIP + "/" + this.createdTripDto.getId())
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)
        ).andDo(res -> {
            TripDto returnedTrip = jsonProcessingUtils.mapFromJson(
                    res.getResponse().getContentAsString(), new TypeReference<TripDto>(){});
            CurrencyDto usdCurrency = (returnedTrip.getCurrencies().stream()
                    .filter(currencyDto -> currencyDto.getName().equals("USD"))
                    .findFirst()).orElse(null);

            assertEquals(200, res.getResponse().getStatus());
            assertNotNull(returnedTrip);
            assertEquals(returnedTrip.getId(), this.createdTripDto.getId());

            assertEquals(returnedTrip.getName(), "Zmeneny nazov");
            assertEquals(returnedTrip.getDescription(), "Zmeneny description");

            assertEquals(returnedTrip.getParticipants().size(), 4);
            assertEquals(returnedTrip.getParticipants().get(0).getName(), "Zmeneny Participant");
            assertEquals(returnedTrip.getParticipants().get(1).getName(), "Andrej2");
            assertEquals(returnedTrip.getParticipants().get(2).getName(), "Andrej3");
            assertEquals(returnedTrip.getParticipants().get(3).getName(), "Dodatocne Doplneny");

            assertEquals(returnedTrip.getTransactions().get(0).getPayerId(), returnedTrip.getParticipants().get(1).getId());
            assertEquals(returnedTrip.getTransactions().get(0).getAmount().longValue(), new BigDecimal(90.58).longValue());
            assertEquals(returnedTrip.getTransactions().get(0).getCategoryId(), returnedTrip.getTransactionCategories().get(1).getId());

            assertEquals(returnedTrip.getTransactions().get(0).getTransactionParticipants().size(), 2);
            assertEquals(returnedTrip.getTransactions().get(0).getTransactionParticipants().get(0).getParticipantId(), returnedTrip.getParticipants().get(0).getId());
            assertEquals(returnedTrip.getTransactions().get(0).getTransactionParticipants().get(0).getAmount().longValue(), new BigDecimal(10.58).longValue());
            assertEquals(returnedTrip.getTransactions().get(0).getTransactionParticipants().get(1).getParticipantId(), returnedTrip.getParticipants().get(2).getId());
            assertEquals(returnedTrip.getTransactions().get(0).getTransactionParticipants().get(1).getAmount().longValue(), new BigDecimal(80).longValue());

            assertEquals(returnedTrip.getTransactions().get(1).getDate().toString(), LocalDate.of(2014, 2, 14).toString());
            assertEquals(returnedTrip.getTransactions().get(1).getCurrencyId(), usdCurrency.getId());
        });
    }

    @Test
    @Order(5)
    public void getTripsOfUser() throws Exception {

        if (this.createdTripDto == null) {
            this.createTrip();
        }

        api.perform(MockMvcRequestBuilders.get(
                RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRIPS_OF_USER + "/" + this.createdUser.getId())
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(res -> {

            assertEquals(200, res.getResponse().getStatus());

            List<TripDto> trips = jsonProcessingUtils.mapFromJson(
                    res.getResponse().getContentAsString(), new TypeReference<List<TripDto>>(){});
            assertNotNull(trips);
            assertEquals(trips.size(), 1);
            assertEquals(trips.get(0).getId(), this.createdTripDto.getId());
        });
    }

    @Test
    @Order(5)
    public void getTripCurrencies() throws Exception {

        if (this.createdTripDto == null) {
            this.createTrip();
        }

        api.perform(MockMvcRequestBuilders.get(
                RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRIP +
                    "/" + this.createdTripDto.getId() + RestApiConst.REST_API_URL_CURRENCIES_OF_TRIP)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(res -> {

            assertEquals(200, res.getResponse().getStatus());

            List<CurrencyDto> currencies = jsonProcessingUtils.mapFromJson(
                    res.getResponse().getContentAsString(), new TypeReference<List<CurrencyDto>>(){});

            assertNotNull(currencies);
            assertEquals(currencies.size(), 3);
            assertTrue(currencies.stream().anyMatch(c -> c.getName().equals("IRR")));
            assertTrue(currencies.stream().anyMatch(c -> c.getName().equals("USD")));
            assertTrue(currencies.stream().anyMatch(c -> c.getName().equals("EUR")));
        });
    }

    @Test
    @Order(7)
    public void deleteTrip() throws Exception {

        if (this.createdTripDto == null) {
            this.createTrip();
        }

        api.perform(MockMvcRequestBuilders.delete(
                RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRIP + "/" + this.createdTripDto.getId())
        ).andDo(res -> {

            assertEquals(200, res.getResponse().getStatus());

            DeleteResourceResponse deletedRes = jsonProcessingUtils.mapFromJson(
                    res.getResponse().getContentAsString(), new TypeReference<DeleteResourceResponse>(){});
            assertEquals(true, deletedRes.isDeleted());

            this.createdTripDto = null;
        });
    }
}
