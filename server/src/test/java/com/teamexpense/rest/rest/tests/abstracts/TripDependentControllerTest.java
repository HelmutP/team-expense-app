package com.teamexpense.rest.rest.tests.abstracts;

import com.fasterxml.jackson.core.type.TypeReference;
import com.teamexpense.rest.constants.RestApiConst;
import com.teamexpense.rest.controllers.TripController;
import com.teamexpense.rest.dtos.database.TripDto;
import com.teamexpense.rest.services.TripService;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@SpringBootTest
public abstract class TripDependentControllerTest extends AbstractControllerTest {

    @Autowired
    private TripController tripControllerMock;

    @Mock
    protected TripService tripService;

    protected MockMvc apiTripController;

    protected TripDto createdSimpleTripDto;
    protected TripDto createdComplexTripDto;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        this.apiTripController = MockMvcBuilders.standaloneSetup(tripControllerMock).build();

        this.createSimpleTrip();
        this.createComplexTrip();
    }

    protected void createSimpleTrip() throws Exception {

        final TripDto trip = this.testDataCreator.initBaseTrip();

        String inputJson = jsonProcessingUtils.mapToJson(trip);

        apiTripController.perform(MockMvcRequestBuilders.post(
                RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRIP_CREATE + "/" + this.createdUser.getId())
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)
        ).andDo(res1 -> {

            if (res1.getResponse().getStatus() != 200) {
                throw new RuntimeException(
                        "HTTP response when creating trip is "
                                + res1.getResponse().getStatus() +
                                ". Expected 200."
                );
            }

            TripDto createdTrip = jsonProcessingUtils.mapFromJson(
                    res1.getResponse().getContentAsString(), new TypeReference<TripDto>(){});

            this.testDataCreator.assignSimpleRelationships(createdTrip);
            this.testDataCreator.updateTripData(createdTrip);

            String inputJsonWithRelations = jsonProcessingUtils.mapToJson(createdTrip);

            apiTripController.perform(MockMvcRequestBuilders.put(
                    RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRIP + "/" + createdTrip.getId())
                    .accept(MediaType.APPLICATION_JSON_VALUE)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content(inputJsonWithRelations)
            ).andDo(res2 -> {

                if (res2.getResponse().getStatus() != 200) {
                    throw new RuntimeException(
                            "HTTP response when creating trip is "
                                    + res2.getResponse().getStatus() +
                                    ". Expected 200."
                    );
                }
                this.createdSimpleTripDto = jsonProcessingUtils.mapFromJson(
                        res2.getResponse().getContentAsString(), new TypeReference<TripDto>(){});
            });
        });
    }

    protected void createComplexTrip() throws Exception {

        final TripDto trip = this.testDataCreator.initBaseTrip();
        this.testDataCreator.addNextTripParticipant(trip);

        String inputJson = jsonProcessingUtils.mapToJson(trip);

        apiTripController.perform(MockMvcRequestBuilders.post(
                RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRIP_CREATE + "/" + this.createdUser.getId())
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)
        ).andDo(res1 -> {

            if (res1.getResponse().getStatus() != 200) {
                throw new RuntimeException(
                        "HTTP response when creating trip is "
                                + res1.getResponse().getStatus() +
                                ". Expected 200."
                );
            }

            TripDto createdTrip = jsonProcessingUtils.mapFromJson(
                    res1.getResponse().getContentAsString(), new TypeReference<TripDto>(){});

            this.testDataCreator.addComplexTransactionsSet(createdTrip);

            String inputJsonWithRelations = jsonProcessingUtils.mapToJson(createdTrip);

            apiTripController.perform(MockMvcRequestBuilders.put(
                    RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRIP + "/" + createdTrip.getId())
                    .accept(MediaType.APPLICATION_JSON_VALUE)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content(inputJsonWithRelations)
            ).andDo(res2 -> {

                if (res2.getResponse().getStatus() != 200) {
                    throw new RuntimeException(
                            "HTTP response when creating trip is "
                                    + res2.getResponse().getStatus() +
                                    ". Expected 200."
                    );
                }
                this.createdComplexTripDto = jsonProcessingUtils.mapFromJson(
                        res2.getResponse().getContentAsString(), new TypeReference<TripDto>(){});
            });
        });
    }
}
