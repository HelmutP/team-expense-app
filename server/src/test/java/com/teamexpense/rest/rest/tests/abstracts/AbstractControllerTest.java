package com.teamexpense.rest.rest.tests.abstracts;

import com.fasterxml.jackson.core.type.TypeReference;
import com.teamexpense.rest.Application;
import com.teamexpense.rest.communication.requests.RegisterUserRequest;
import com.teamexpense.rest.constants.RestApiConst;
import com.teamexpense.rest.controllers.UserController;
import com.teamexpense.rest.data.TestDataCreator;
import com.teamexpense.rest.dtos.database.UserDto;
import com.teamexpense.rest.services.UserService;
import com.teamexpense.rest.utils.JsonProcessingUtils;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public abstract class AbstractControllerTest {

    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Autowired
    protected JsonProcessingUtils jsonProcessingUtils;

    @Autowired
    protected TestDataCreator testDataCreator;

    @Autowired
    private UserController userControllerMock;

    @Mock
    protected UserService userService;

    protected MockMvc api;
    protected MockMvc apiUserController;

    protected UserDto createdUser;

    protected void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        this.apiUserController = MockMvcBuilders.standaloneSetup(userControllerMock).build();

        this.createUser();
    }

    private void createUser() throws Exception {

        final RegisterUserRequest request = this.testDataCreator.createUserRegisterReq();
        final String inputJson = jsonProcessingUtils.mapToJson(request);

        apiUserController.perform(MockMvcRequestBuilders.post(RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_USER_REGISTER)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)
        ).andDo(res -> {
            assertEquals(200, res.getResponse().getStatus());

            final UserDto registeredUser = jsonProcessingUtils.mapFromJson(
                    res.getResponse().getContentAsString(), new TypeReference<UserDto>(){});

            this.createdUser = registeredUser;
        });
    }

    public abstract void testConnection() throws Exception;
}
