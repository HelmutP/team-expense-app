package com.teamexpense.rest.rest.tests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.teamexpense.rest.communication.responses.DeleteResourceResponse;
import com.teamexpense.rest.constants.RestApiConst;
import com.teamexpense.rest.controllers.TransactionController;
import com.teamexpense.rest.dtos.database.TransactionDto;
import com.teamexpense.rest.dtos.database.TransactionParticipantDto;
import com.teamexpense.rest.rest.tests.abstracts.TripDependentControllerTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SpringBootTest
public class TransactionControllerTest extends TripDependentControllerTest {

    @Autowired
    private TransactionController transactionController;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        this.api = MockMvcBuilders.standaloneSetup(transactionController).build();
    }

    @Test
    @Order(1)
    public void testConnection() throws Exception {
        api.perform(MockMvcRequestBuilders.options(RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRANSACTION)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(res -> {
            assertEquals(200, res.getResponse().getStatus());
        });
    }

    @Test
    @Order(2)
    public void addTransaction() throws Exception {

        final TransactionDto existingTransaction = this.createdSimpleTripDto.getTransactions().get(0);

        final ArrayList<TransactionParticipantDto> newTransactionParticipants = new ArrayList<>();
        newTransactionParticipants.add(new TransactionParticipantDto(new BigDecimal(124.5), existingTransaction.getPayerId()));
        final TransactionDto newTransaction = new TransactionDto(
                "Pridana transakcia", "Popis pridanej transakcie", new BigDecimal(124.5),
                existingTransaction.getCurrencyId(), existingTransaction.getPayerId(), LocalDate.now(), newTransactionParticipants,
                existingTransaction.getCategoryId()
        );
        newTransaction.setCategoryId(this.createdSimpleTripDto.getTransactionCategories().get(0).getId());
        newTransaction.setTripId(this.createdSimpleTripDto.getId());

        final String inputJson = jsonProcessingUtils.mapToJson(newTransaction);

        api.perform(MockMvcRequestBuilders.post(RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRANSACTION)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)
        ).andDo(res -> {

            assertEquals(200, res.getResponse().getStatus());
            assertNotNull(res.getResponse().getContentAsString());

            final TransactionDto createdTransaction = jsonProcessingUtils.mapFromJson(
                    res.getResponse().getContentAsString(), new TypeReference<TransactionDto>(){});

            assertEquals(createdTransaction.getName(), "Pridana transakcia");
            assertEquals(createdTransaction.getDescription(), "Popis pridanej transakcie");
            assertEquals(createdTransaction.getAmount().longValue(), new BigDecimal(124.5).longValue());
            assertEquals(createdTransaction.getCurrencyId(), existingTransaction.getCurrencyId());
            assertEquals(createdTransaction.getPayerId(), existingTransaction.getPayerId());
            assertEquals(createdTransaction.getTripId(), existingTransaction.getTripId());
            assertEquals(createdTransaction.getCategoryId(), this.createdSimpleTripDto.getTransactionCategories().get(0).getId());

            assertEquals(createdTransaction.getTransactionParticipants().size(), 1);
            assertEquals(createdTransaction.getTransactionParticipants().get(0).getAmount().longValue(), new BigDecimal(124.5).longValue());
            assertEquals(createdTransaction.getTransactionParticipants().get(0).getParticipantId(), existingTransaction.getPayerId());
        });
    }

    @Test
    @Order(3)
    public void getTransaction() throws Exception {

        api.perform(MockMvcRequestBuilders.get(
                RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRANSACTION +
                        "/" + this.createdSimpleTripDto.getTransactions().get(this.createdSimpleTripDto.getTransactions().size() - 1).getId()
                ).accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(res -> {

            assertEquals(200, res.getResponse().getStatus());
            assertNotNull(res.getResponse().getContentAsString());

            final TransactionDto createdTransaction = jsonProcessingUtils.mapFromJson(
                    res.getResponse().getContentAsString(), new TypeReference<TransactionDto>(){});

            assertEquals(createdTransaction.getName(), "Druha transakcia");
            assertEquals(createdTransaction.getDescription(), "IRR as own currency name");
            assertEquals(createdTransaction.getAmount().longValue(), new BigDecimal(1899).longValue());
            assertEquals(createdTransaction.getCategoryId(), 0);

            assertEquals(createdTransaction.getTransactionParticipants().size(), 1);
            assertEquals(createdTransaction.getTransactionParticipants().get(0).getAmount().longValue(), new BigDecimal(1899).longValue());
            assertEquals(createdTransaction.getTransactionParticipants().get(0).getParticipantId(), createdTransaction.getPayerId());
        });
    }

    @Test
    @Order(4)
    public void updateTransaction() throws Exception {

        api.perform(MockMvcRequestBuilders.get(
                RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRANSACTION +
                        "/" + this.createdSimpleTripDto.getTransactions().get(this.createdSimpleTripDto.getTransactions().size() - 1).getId()
                        ).accept(MediaType.APPLICATION_JSON_VALUE)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(resGet -> {

            assertEquals(200, resGet.getResponse().getStatus());
            assertNotNull(resGet.getResponse().getContentAsString());

            final TransactionDto retrievedTransaction = jsonProcessingUtils.mapFromJson(
                    resGet.getResponse().getContentAsString(), new TypeReference<TransactionDto>(){});

            retrievedTransaction.setName("Zmenena transakcia");
            retrievedTransaction.setDescription("Zmenena transakcia desc");
            retrievedTransaction.setAmount(new BigDecimal(2000));
            retrievedTransaction.setCategoryId(this.createdSimpleTripDto.getTransactionCategories().get(2).getId());
            retrievedTransaction.getTransactionParticipants().clear();
            retrievedTransaction.addTransactionParticipant(this.createdSimpleTripDto.getParticipants().get(0).getId(), new BigDecimal(1500));
            retrievedTransaction.addTransactionParticipant(this.createdSimpleTripDto.getParticipants().get(1).getId(), new BigDecimal(500));

            final String updateJson = jsonProcessingUtils.mapToJson(retrievedTransaction);

            api.perform(MockMvcRequestBuilders.put(
                    RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRANSACTION +
                            "/" + this.createdSimpleTripDto.getTransactions().get(this.createdSimpleTripDto.getTransactions().size() - 1).getId()
                    ).accept(MediaType.APPLICATION_JSON_VALUE)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content(updateJson)
            ).andDo(resUpdate -> {

                assertEquals(200, resUpdate.getResponse().getStatus());
                assertNotNull(resUpdate.getResponse().getContentAsString());

                final TransactionDto updatedTransaction = jsonProcessingUtils.mapFromJson(
                        resUpdate.getResponse().getContentAsString(), new TypeReference<TransactionDto>(){});

                assertEquals(updatedTransaction.getName(), "Zmenena transakcia");
                assertEquals(updatedTransaction.getDescription(), "Zmenena transakcia desc");
                assertEquals(updatedTransaction.getAmount().longValue(), new BigDecimal(2000).longValue());
                assertEquals(updatedTransaction.getCategoryId(), this.createdSimpleTripDto.getTransactionCategories().get(2).getId());

                assertEquals(updatedTransaction.getTransactionParticipants().size(), 2);
                assertEquals(updatedTransaction.getTransactionParticipants().get(0).getAmount().longValue(), new BigDecimal(1500).longValue());
                assertEquals(updatedTransaction.getTransactionParticipants().get(0).getParticipantId(), this.createdSimpleTripDto.getParticipants().get(0).getId());
                assertEquals(updatedTransaction.getTransactionParticipants().get(1).getAmount().longValue(), new BigDecimal(500).longValue());
                assertEquals(updatedTransaction.getTransactionParticipants().get(1).getParticipantId(), this.createdSimpleTripDto.getParticipants().get(1).getId());
            });
        });
    }

    @Test
    @Order(5)
    public void deleteTransaction() throws Exception {

        api.perform(MockMvcRequestBuilders.delete(
                RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRANSACTION +
                        "/" + this.createdSimpleTripDto.getTransactions().get(this.createdSimpleTripDto.getTransactions().size() - 1).getId()
                     ).accept(MediaType.APPLICATION_JSON_VALUE)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(res -> {

            assertEquals(200, res.getResponse().getStatus());

            DeleteResourceResponse deletedRes = jsonProcessingUtils.mapFromJson(
                    res.getResponse().getContentAsString(), new TypeReference<DeleteResourceResponse>(){});
            assertEquals(true, deletedRes.isDeleted());
        });
    }

    @Test
    @Order(6)
    public void notExistingTransactionIdToDelete() throws Exception {

        api.perform(MockMvcRequestBuilders.delete(
                RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRANSACTION +
                        "/" + "0"
                ).accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(res -> {
            assertEquals(res.getResponse().getStatus(), 400);
        });
    }

    @Test
    @Order(7)
    public void getLatestTransactions() throws Exception {

        api.perform(MockMvcRequestBuilders.get(
                RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRANSACTION_LATEST +
                        "/" + this.createdComplexTripDto.getId()
                ).accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(res -> {

            assertEquals(200, res.getResponse().getStatus());

            final List<TransactionDto> latestTransactions = jsonProcessingUtils.mapFromJson(
                    res.getResponse().getContentAsString(), new TypeReference<List<TransactionDto>>(){});

            assertNotNull(latestTransactions);
            assertEquals(latestTransactions.size(), 3);

            assertEquals(latestTransactions.get(0).getDate().toString(), this.createdComplexTripDto.getTransactions().get(3).getDate().toString());
            assertEquals(latestTransactions.get(0).getName(), this.createdComplexTripDto.getTransactions().get(3).getName());

            assertEquals(latestTransactions.get(1).getDate().toString(), this.createdComplexTripDto.getTransactions().get(1).getDate().toString());
            assertEquals(latestTransactions.get(1).getName(), this.createdComplexTripDto.getTransactions().get(1).getName());

            assertEquals(latestTransactions.get(2).getDate().toString(), this.createdComplexTripDto.getTransactions().get(0).getDate().toString());
            assertEquals(latestTransactions.get(2).getName(), this.createdComplexTripDto.getTransactions().get(0).getName());
        });
    }

    @Test
    @Order(8)
    public void getTripTransactionsByTexSearch() throws Exception {

        final String searchedText = "Westminister";

        api.perform(MockMvcRequestBuilders.post(
                RestApiConst.REST_API_URL_PREFIX + RestApiConst.REST_API_URL_TRANSACTION_SEARCH +
                        "/" + this.createdComplexTripDto.getId()
                ).accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(searchedText)
        ).andDo(res -> {

            assertEquals(200, res.getResponse().getStatus());

            final List<TransactionDto> filteredTransactions = jsonProcessingUtils.mapFromJson(
                    res.getResponse().getContentAsString(), new TypeReference<List<TransactionDto>>(){});

            assertNotNull(filteredTransactions);
            assertEquals(filteredTransactions.size(), 1);

            assertEquals(filteredTransactions.get(0).getName(), this.createdComplexTripDto.getTransactions().get(9).getName());
            assertEquals(filteredTransactions.get(0).getDescription(), this.createdComplexTripDto.getTransactions().get(9).getDescription());
            assertEquals(filteredTransactions.get(0).getId(), this.createdComplexTripDto.getTransactions().get(9).getId());
        });
    }
}
